use core::convert::Infallible;
use std::{collections::HashSet, sync::Arc};

use futures::stream::{FuturesUnordered, StreamExt};
use miette::Diagnostic;
use thiserror::Error;
use tokio::{
    sync::{mpsc, oneshot, Mutex},
    task::{self, JoinHandle},
};
use tokio_util::compat::TokioAsyncReadCompatExt;

use maptool::{DirectServerAddress, ServerConnection, WebRtcServerAddress};
use maptool_agent_api::{
    zbus::{
        self, dbus_interface, fdo,
        names::{BusName, OwnedUniqueName},
        zvariant::{ObjectPath, Optional, OwnedObjectPath},
        Connection as ZBusConnection, ConnectionBuilder, Error as ZBusError,
        Interface, MessageHeader, ProxyDefault, SignalContext,
    },
    ConnectionDisconnectError, ConnectionHandshakeError, ManagerConnectError,
    MapToolAgentManager1Proxy, MessageTaskReadError, OwnedDirectServerAddress,
    OwnedWebRtcServerAddress, SendRawMessageError, SubscribeError,
};

enum SessionMsg {
    NewPeer(OwnedUniqueName),
    SendRawMessage {
        msg: Vec<u8>,
        response_tx: oneshot::Sender<Result<Vec<u8>, maptool::handshake::WriteMsgError>>,
    },
    Disconnect,
}

struct MapToolAgentSession1 {
    session_tx: mpsc::Sender<SessionMsg>,
}

#[dbus_interface(name = "io.gitlab.richardmaw.MapToolAgent.Session1")]
impl MapToolAgentSession1 {
    /// Subscribe to messages
    async fn subscribe(
        &self,
        #[zbus(header)] hdr: MessageHeader<'_>,
    ) -> Result<(), SubscribeError> {
        use SubscribeError::*;
        let sender = hdr.sender()?.ok_or(NoPeerUniqueName)?.to_owned();
        self.session_tx
            .send(SessionMsg::NewPeer(sender.into()))
            .await
            .map_err(|_| ())
            .expect("Receiver task should outlive MapToolAgentSession1");
        Ok(())
    }

    /// Raw serialized messages from MapTool server
    #[dbus_interface(signal)]
    async fn raw_message(signal_ctxt: &SignalContext<'_>, frame: &[u8]) -> zbus::Result<()>;

    /// Terminating error for read stream from server.
    #[dbus_interface(signal)]
    async fn read_error(
        signal_ctxt: &SignalContext<'_>,
        error: MessageTaskReadError,
    ) -> zbus::Result<()>;

    async fn send_raw_message(&self, frame: Vec<u8>) -> Result<(), SendRawMessageError> {
        use SendRawMessageError::*;
        let (response_tx, response_rx) = oneshot::channel();
        self.session_tx
            .send(SessionMsg::SendRawMessage {
                msg: frame,
                response_tx,
            })
            .await
            .map_err(|_| ())
            .expect("Receiver task should outlive MapToolAgentSession1");
        let _ = response_rx
            .await
            .expect("Connection task should always send a response")
            .map_err(|e| WriteMsgError(e.to_string()))?;
        Ok(())
    }

    async fn disconnect(&self) -> fdo::Result<()> {
        self.session_tx
            .send(SessionMsg::Disconnect)
            .await
            .map_err(|_| ())
            .expect("Receiver task should outlive MapToolAgentSession1");
        Ok(())
    }
}

enum ConnectionMsg {
    Disconnect,
    Handshake {
        name: String,
        password: Option<Vec<u8>>,
        public_key: Option<Vec<u8>>,
        private_key: Option<Vec<u8>>,
        response_tx: oneshot::Sender<Result<(), ConnectionHandshakeError>>,
    },
}

struct MapToolAgentConnection1 {
    // handshake consumes the connection to return a session
    // but dbus can't guarantee no concurrent calls
    // so we lock and take the connection object
    connect_tx: Arc<Mutex<Option<oneshot::Sender<ConnectionMsg>>>>,
}
impl MapToolAgentConnection1 {
    fn new(connect_tx: oneshot::Sender<ConnectionMsg>) -> Self {
        Self {
            connect_tx: Arc::new(Mutex::new(Some(connect_tx))),
        }
    }
}
#[dbus_interface(name = "io.gitlab.richardmaw.MapToolAgent.Connection1")]
impl MapToolAgentConnection1 {
    #[dbus_interface(signal)]
    async fn pin(signal_ctxt: &SignalContext<'_>, pin: &str) -> zbus::Result<()>;

    async fn handshake(
        &self,
        name: &str,
        password: Optional<Vec<u8>>,
        public_key: Optional<Vec<u8>>,
        private_key: Optional<Vec<u8>>,
        #[zbus(header)] hdr: MessageHeader<'_>,
    ) -> Result<OwnedObjectPath, ConnectionHandshakeError> {
        use ConnectionHandshakeError::*;
        let path = hdr.path()?.unwrap();
        let mut conn_cell = self.connect_tx.lock().await;
        if conn_cell.is_none() {
            return Err(AlreadyConnected);
        }

        let name = name.into();
        let password: Option<Vec<u8>> = password.into();
        let public_key: Option<Vec<u8>> = public_key.into();
        let private_key: Option<Vec<u8>> = private_key.into();

        let connect_tx = conn_cell
            .take()
            .expect("Should be initialized with Some and we checked if none while locked");

        let (response_tx, response_rx) = oneshot::channel();
        connect_tx
            .send(ConnectionMsg::Handshake {
                name,
                password,
                public_key,
                private_key,
                response_tx,
            })
            .map_err(|_| ())
            .expect("Connection task should outlive interface");

        response_rx
            .await
            .expect("Connection task should outlive interface")?;

        Ok(OwnedObjectPath::from(path.to_owned()))
    }

    async fn disconnect(&self) -> Result<(), ConnectionDisconnectError> {
        use ConnectionDisconnectError::*;
        let mut conn_cell = self.connect_tx.lock().await;
        if conn_cell.is_none() {
            return Err(AlreadyConnected);
        }

        let connect_tx = conn_cell
            .take()
            .expect("Should be initialized with Some and we checked if none while locked");
        connect_tx
            .send(ConnectionMsg::Disconnect)
            .map_err(|_| ())
            .expect("Receiver task should outlive MapToolAgentConnection1");
        Ok(())
    }
}

#[derive(Debug, Diagnostic, Error)]
pub(crate) enum ConnectionTaskError {
    #[error("Failed to look-up bound interface")]
    ObjectLookupFailed(#[source] ZBusError),
    #[error("Failed to bind connection interface")]
    ConnectionBindFailed(#[source] ZBusError),
    #[error("Failed to start connection: {0}")]
    StartConnectionTaskFailed(String),
    #[error("Failed to handshake")]
    HandshakeFailed(
        #[source] maptool::handshake::HandshakeError<ZBusError, Infallible, Infallible>,
    ),
    #[error("Failed to bind session interface")]
    SessionBindFailed(#[source] ZBusError),
    #[error("Failed to unbind connection interface")]
    ConnectionUnbindFailed(#[source] ZBusError),
    #[error("Failed to unbind session interface")]
    SessionUnbindFailed(#[source] ZBusError),
    #[error("Failed to read messages")]
    MessageTaskReadFailed(#[source] MessageTaskReadError),
}

async fn connection_task(
    manager_path: OwnedObjectPath,
    bus: ZBusConnection,
    conn: maptool::ServerConnection,
    started_tx: oneshot::Sender<Result<OwnedObjectPath, ZBusError>>,
) -> Result<(), ConnectionTaskError> {
    use ConnectionTaskError::*;

    let object_server = bus.object_server();
    let (connect_tx, connect_rx) = oneshot::channel();
    let interface = MapToolAgentConnection1::new(connect_tx);
    let started_res = loop {
        let id: u64 = rand::random();
        let path: OwnedObjectPath = format!("{}/connections/{}", manager_path.as_ref(), id)
            .try_into()
            .expect("ObjectPath construction should be valid");
        log::debug!(
            "Attempting to bind connection interface to object {:?}",
            &path
        );
        match object_server
            .interface::<_, MapToolAgentConnection1>(&path)
            .await
        {
            Ok(_) => continue,
            Err(ZBusError::InterfaceNotFound) => (),
            Err(e) => break Err(e),
        }
        // TODO: ObjectServer::at will return Ok(false) if there was already something there
        // and not insert the object at that interface.
        // Since it takes the object by value and doesn't return it
        // we can't use this API to retry with another path.
        assert!(object_server.at(&path, interface).await.map_err(ConnectionBindFailed)?,
                "Object was registered at interface between checking and connection was instead destroyed");
        log::debug!(
            "Successfully bound connection interface to object {:?}",
            &path
        );
        break Ok(path);
    };
    let res = match &started_res {
        Err(e) => Err(StartConnectionTaskFailed(e.to_string())),
        Ok(v) => Ok(v.clone()),
    };
    started_tx
        .send(started_res)
        .expect("Manager connect handler should remain alive until connect started");
    let path = res?;

    let msg = connect_rx
        .await
        .expect("Connection interface should have shorter life than task");
    let (name, password, public_key, private_key, handshake_res_tx) = match msg {
        ConnectionMsg::Disconnect => return Ok(()),
        ConnectionMsg::Handshake {
            name,
            password,
            public_key,
            private_key,
            response_tx,
        } => (name, password, public_key, private_key, response_tx),
    };

    let interface = object_server
        .interface::<_, MapToolAgentConnection1>(&path)
        .await
        .map_err(ObjectLookupFailed)?;

    let key_options = match (public_key, private_key) {
        (Some(public_key), Some(private_key)) => Some(maptool::handshake::KeyOptions {
            public_key,
            private_key_fn: || async { Ok::<_, Infallible>(private_key) },
            report_pin_fn: move |pin| {
                // TODO: This copy is needed because the closure returns a future
                // and the function type signature doesn't specify it has the lifetime of pin: &str,
                // which would assure the future is awaited while pin is alive,
                // and I can't work out how you could do that.
                let pin = pin.to_owned();
                async move {
                    let ctx = interface.signal_context();
                    MapToolAgentConnection1::pin(&ctx, &pin).await?;
                    Ok::<_, ZBusError>(())
                }
            },
        }),
        _ => None,
    };

    let session = match maptool::handshake::handshake(
        conn.compat(),
        name.into(),
        key_options,
        password.map(|pw| move || async { Ok::<_, Infallible>(pw) }),
    )
    .await
    {
        Ok((_, session)) => {
            handshake_res_tx
                .send(Ok(()))
                .map_err(|_| ())
                .expect("Interface should wait for response");
            Ok(session)
        }
        Err(e) => {
            handshake_res_tx
                .send(Err(ConnectionHandshakeError::HandshakeError(e.to_string())))
                .map_err(|_| ())
                .expect("Interface should wait for response");
            Err(HandshakeFailed(e))
        }
    }?;
    // TODO: If connection is recoverable then replace it into conn cell

    let (session_tx, mut session_rx) = mpsc::channel(1);
    let (mut session_read, mut session_write) = session.split();

    let dbus = fdo::DBusProxy::new(&bus)
        .await
        .expect("D-Bus daemon proxy should always be creatable");
    let mut name_lost_stream = dbus
        .receive_name_lost()
        .await
        .expect("DBus should provide NameLost signals");
    let mut peers = HashSet::new();

    object_server
        .at(path.as_ref(), MapToolAgentSession1 { session_tx })
        .await
        .map_err(SessionBindFailed)?;
    object_server
        .remove::<MapToolAgentConnection1, _>(path.as_ref())
        .await
        .map_err(ConnectionUnbindFailed)?;
    let interface = object_server
        .interface::<_, MapToolAgentSession1>(&path)
        .await
        .expect(
            "MapToolAgentSession1 object should be bound immediately after a client subscribes",
        );
    let sctx = interface.signal_context();

    let res = 'main: loop {
        let peer = match session_rx
            .recv()
            .await
            .expect("MapToolAgentSession1 should remain alive until explicitly removed")
        {
            SessionMsg::Disconnect => break 'main Ok(()),
            SessionMsg::NewPeer(peer) => peer,
            SessionMsg::SendRawMessage { msg, response_tx } => {
                match session_write.write_msg(msg).await {
                    Ok(v) => {
                        response_tx
                            .send(Ok(v))
                            .map_err(|_| ())
                            .expect("send_raw_message handler should be alive until response");
                        continue;
                    }
                    Err(e) => {
                        response_tx
                            .send(Err(e))
                            .map_err(|_| ())
                            .expect("send_raw_message handler should be alive until response");
                        break Err(MessageTaskReadError::WriteError);
                    }
                }
            }
        };
        peers.insert(peer);

        'message: loop {
            // TODO: What is making read_msg Unpin?
            let mut read_fut = Box::pin(session_read.read_msg(Vec::new()));
            tokio::select! {
                res = &mut read_fut => {
                    let msg = match res {
                        Err(_e) => break 'main Err(MessageTaskReadError::ReadError),
                        Ok(msg) => msg,
                    };
                    // TODO: When Session is changed to implement message type
                    // reserialize to send as raw message
                    // and dispatch on the signal handlers.
                    // This is because we want the underlying Session type to guarantee
                    // we're only sending messages we expect,
                    // but raw messages are more useful to a client that can use GPL
                    // since then it can communicate in any type
                    // not just the ones I've written bindings for.
                    MapToolAgentSession1::raw_message(&sctx, &msg).await.expect("Bus should be operable for longer than connection");

                    drop(read_fut);
                    read_fut = Box::pin(session_read.read_msg(msg));
                },
                msg = session_rx.recv() => {
                    let msg = msg.expect("MapToolAgentSession1 should remain alive until explicitly removed");
                    match msg {
                        SessionMsg::Disconnect => {
                            break 'main Ok(());
                        }
                        SessionMsg::NewPeer(peer) => {
                            peers.insert(peer);
                        }
                        SessionMsg::SendRawMessage { msg, response_tx } => {
                            match session_write.write_msg(msg).await {
                                Ok(v) => {
                                    response_tx
                                        .send(Ok(v))
                                        .map_err(|_| ())
                                        .expect("send_raw_message handler should be alive until response");
                                    continue;
                                }
                                Err(e) => {
                                    response_tx
                                        .send(Err(e))
                                        .map_err(|_| ())
                                        .expect("send_raw_message handler should be alive until response");
                                    break 'main Err(MessageTaskReadError::WriteError);
                                }
                            }
                        }
                    }
                }
                name_lost = name_lost_stream.next() => {
                    let name_lost = name_lost.expect("DBus connection should be alive and producing names");
                    let args = match name_lost.args() {
                        Err(_e) => break 'main Err(MessageTaskReadError::NameLostSignalReadError),
                        Ok(args) => args,
                    };
                    let name = match args.name {
                        BusName::Unique(name) => name,
                        _ => continue,
                    };
                    peers.remove(&OwnedUniqueName::from(name.into_owned()));
                    if peers.is_empty() {
                        break 'message;
                    }
                }
            }
        }
    };
    if let Err(e) = &res {
        MapToolAgentSession1::read_error(&sctx, e.clone())
            .await
            .expect("Bus should be operable for longer than connection");
    }

    object_server
        .remove::<MapToolAgentSession1, _>(path.as_ref())
        .await
        .map_err(SessionUnbindFailed)?;

    res.map_err(MessageTaskReadFailed)
}

enum ManagerMsg {
    NewConnection(JoinHandle<Result<(), ConnectionTaskError>>),
    Shutdown,
}

struct MapToolAgentManager1 {
    manager_tx: mpsc::Sender<ManagerMsg>,
}

impl MapToolAgentManager1 {
    async fn register_connection(
        &self,
        path: &ObjectPath<'_>,
        bus: &ZBusConnection,
        conn: maptool::ServerConnection,
    ) -> Result<OwnedObjectPath, ZBusError> {
        let (started_tx, started_rx) = oneshot::channel();
        let task = tokio::spawn(connection_task(
            path.to_owned().into(),
            bus.clone(),
            conn,
            started_tx,
        ));
        self.manager_tx
            .send(ManagerMsg::NewConnection(task))
            .await
            .map_err(|e| e.to_string())
            .expect("Manager task should outlive Manager interface");
        started_rx
            .await
            .expect("Connection task should always send a result")
    }
}

#[dbus_interface(name = "io.gitlab.richardmaw.MapToolAgent.Manager1")]
impl MapToolAgentManager1 {
    async fn connect_direct(
        &self,
        address: OwnedDirectServerAddress<String>,
        #[zbus(header)] hdr: MessageHeader<'_>,
        #[zbus(connection)] bus: &ZBusConnection,
    ) -> Result<OwnedObjectPath, ManagerConnectError> {
        use ManagerConnectError::*;
        let path = hdr.path()?.unwrap();
        log::debug!("{}::connect_direct", &path);
        let addr: DirectServerAddress<_> = (&address).into();
        let conn = ServerConnection::connect(addr)
            .await
            .map_err(|e| ConnectError(e.to_string()))?;
        let path = self.register_connection(path, bus, conn).await?;
        log::debug!("Returning path {:?}", &path);
        Ok(path)
    }

    async fn connect_webrtc(
        &self,
        address: OwnedWebRtcServerAddress<String, String>,
        #[zbus(header)] hdr: MessageHeader<'_>,
        #[zbus(connection)] bus: &ZBusConnection,
    ) -> Result<OwnedObjectPath, ManagerConnectError> {
        use ManagerConnectError::*;
        let path = hdr.path()?.unwrap();
        log::debug!("{}::connect_webrtc", &path);
        let addr: WebRtcServerAddress<_, _> = (&address).into();
        let conn = ServerConnection::connect(addr)
            .await
            .map_err(|e| ConnectError(e.to_string()))?;
        log::debug!("{}::connect_webrtc connected", &path);
        let path = self.register_connection(path, bus, conn).await?;
        log::debug!("Returning path {:?}", &path);
        Ok(path)
    }

    async fn shutdown(&self) -> fdo::Result<()> {
        self.manager_tx
            .send(ManagerMsg::Shutdown)
            .await
            .map_err(|e| e.to_string())
            .expect("Manager task should outlive Manager interface");
        Ok(())
    }

    async fn source_offer(&self) -> fdo::Result<Vec<u8>> {
        Ok(maptool::SOURCE_OFFER.into())
    }
}

#[derive(Debug, Diagnostic, Error)]
pub(crate) enum ServeError {
    #[error("Failed to connect to bus")]
    ConnectionFailure(#[source] ZBusError),
    #[error("Failed to serve interface")]
    ServeInterfaceFailure(#[source] ZBusError),
    #[error("Failed to serve at well-known name")]
    ServeWellKnownFailure(#[source] ZBusError),
    #[error("Connection task ended in panic")]
    ConnectionTaskPanic(#[source] task::JoinError),
    #[error("Connection task ended in error")]
    ConnectionTaskError(#[source] ConnectionTaskError),
    #[error("Failed to remove manager interface")]
    RemoveManagerInterfaceFailed(#[source] ZBusError),
}
pub(crate) async fn serve(
    bus_address: Option<&str>,
    well_known_name: &str,
    object_path: &str,
) -> Result<(), ServeError> {
    use ServeError::*;
    assert_eq!(
        MapToolAgentManager1Proxy::INTERFACE,
        MapToolAgentManager1::name().as_str(),
    );

    let (manager_tx, mut manager_rx) = mpsc::channel(1);
    // TODO: Make backlog configurable?
    let mut connection_tasks = FuturesUnordered::new();
    let manager = MapToolAgentManager1 { manager_tx };

    let conn = match bus_address {
        Some(addr) => {
            log::debug!("Connecting to bus {}", &addr);
            ConnectionBuilder::address(addr)
        }
        None => {
            log::debug!("Connecting to session bus");
            ConnectionBuilder::session()
        }
    }
    .map_err(ConnectionFailure)?
    .internal_executor(false)
    .serve_at(object_path, manager)
    .map_err(ServeInterfaceFailure)?
    .name(well_known_name)
    .map_err(ServeWellKnownFailure)?
    .build()
    .await
    .map_err(ConnectionFailure)?;

    log::debug!("Serving io.gitlab.richardmaw.MapToolAgent.Manager1");

    let msg = loop {
        tokio::select! {
            _ = conn.executor().tick() => continue,
            msg = manager_rx.recv() => {
                let msg = msg.expect("manager_tx is only closed when this task removes the interface");
                break msg;
            }
        }
    };
    let task = match msg {
        ManagerMsg::Shutdown => {
            // We have no ongoing tasks so dropping connection should shutdown
            // TODO: Confirm this
            return Ok(());
        }
        ManagerMsg::NewConnection(task) => task,
    };
    connection_tasks.push(task);

    // Handle manager messages and connection task finishes
    // until we get Shutdown or no more connection tasks
    loop {
        let msg = tokio::select! {
            _ = conn.executor().tick() => continue,
            msg = manager_rx.recv() => {
                msg.expect("manager_tx is only closed when this task removes the interface")
            }
            res = connection_tasks.next() => {
                let res = res.expect("Should only poll tasks after adding one and break loop as soon as the task set is empty");
                let _ = res.map_err(ConnectionTaskPanic)?.map_err(ConnectionTaskError)?;
                if connection_tasks.is_empty() {
                    break;
                }
                continue
            }
        };
        let task = match msg {
            ManagerMsg::Shutdown => break,
            ManagerMsg::NewConnection(task) => task,
        };
        connection_tasks.push(task);
    }

    let object_server = conn.object_server();
    object_server
        .remove::<MapToolAgentManager1, _>(object_path)
        .await
        .map_err(RemoveManagerInterfaceFailed)?;

    // Drain all remaining manager messages
    loop {
        let msg = tokio::select! {
            _ = conn.executor().tick() => continue,
            // We might have had in-flight connections after shutdown to wait for
            msg = manager_rx.recv() => {
                match msg {
                    // Drained all messages
                    None => break,
                    Some(msg) => msg,
                }
            }
        };
        match msg {
            ManagerMsg::NewConnection(task) => connection_tasks.push(task),
            // Ignore shutdowns
            _ => (),
        }
    }

    // Drain all tasks
    loop {
        tokio::select! {
            _ = conn.executor().tick() => continue,
            res = connection_tasks.next() => {
                let res = match res {
                    None => break,
                    Some(res) => res,
                };
                let _ = res.map_err(ConnectionTaskPanic)?.map_err(ConnectionTaskError)?;
                if connection_tasks.is_empty() {
                    break;
                }
            }
        };
    }

    Ok(())
}
