use miette::Diagnostic;
use thiserror::Error;

use maptool_agent_api::{
    zbus::{ConnectionBuilder, Error as ZBusError},
    MapToolAgentSession1Proxy,
};

#[derive(Debug, Diagnostic, Error)]
pub(crate) enum SendRawMessageError {
    #[error("Failed to connect to bus")]
    DBusConnectionFailure(#[source] ZBusError),
    #[error("Failed to set object destination")]
    SetDestinationFailure(#[source] ZBusError),
    #[error("Failed to set object path")]
    SetPathFailure(#[source] ZBusError),
    #[error("Failed to create session proxy")]
    CreateSessionProxyFailure(#[source] ZBusError),
    #[error("Failed to send raw message")]
    SendRawMessageFailed(#[source] maptool_agent_api::SendRawMessageError),
}
pub(crate) async fn send_raw_message(
    bus_address: Option<&str>,
    well_known_name: &str,
    object_path: &str,
    input: Vec<u8>,
) -> Result<(), SendRawMessageError> {
    use SendRawMessageError::*;
    let conn = match bus_address {
        Some(addr) => {
            log::debug!("Connecting to bus {}", &addr);
            ConnectionBuilder::address(addr)
        }
        None => {
            log::debug!("Connecting to session bus");
            ConnectionBuilder::session()
        }
    }
    .map_err(DBusConnectionFailure)?
    .internal_executor(false)
    .build()
    .await
    .map_err(DBusConnectionFailure)?;

    let mut send = Box::pin(async {
        let session = MapToolAgentSession1Proxy::builder(&conn)
            .destination(well_known_name)
            .map_err(SetDestinationFailure)?
            .path(object_path)
            .map_err(SetPathFailure)?
            .build()
            .await
            .map_err(CreateSessionProxyFailure)?;

        session
            .send_raw_message(input)
            .await
            .map_err(SendRawMessageFailed)
    });

    loop {
        tokio::select! {
            v = &mut send => break v,
            _ = conn.executor().tick() => (),
        }
    }
}
