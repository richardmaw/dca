use std::ffi::OsString;

use clap::{Args, Parser};
use miette::{IntoDiagnostic, Result};

use maptool_agent_api::{zbus::ProxyDefault, MapToolAgentConnection1Proxy, MapToolAgentManager1Proxy};

#[cfg(feature = "install")]
use crate::install;

#[cfg(feature = "install")]
#[derive(Debug, Args)]
pub(crate) struct Install {
    /// Abort installation if a file already exists
    #[clap(short, long)]
    no_clobber: bool,
    /// Install with hardlinks instead of symlinks.
    /// This would cause installation to outlive `cargo uninstall`
    /// which may not be what you want, but may save disk space.
    #[clap(short = 'H', long)]
    hardlink: bool,
}

#[cfg(feature = "install")]
impl Install {
    fn run(self) -> Result<()> {
        install::install_bash_completions(!self.no_clobber)?;
        let links_dir = install::install_links(!self.hardlink, !self.no_clobber)?;
        install::install_service(!self.no_clobber, &links_dir)?;
        Ok(())
    }
}

#[derive(Debug, Args)]
pub(crate) struct Bus {
    /// Alternative bus to connect to instead of session bus
    #[clap(long)]
    bus_address: Option<String>,
}

#[derive(Debug, Args)]
pub(crate) struct ManagerAddress {
    #[clap(flatten)]
    bus: Bus,
    /// Well-known name of the manager
    #[clap(long, default_value = MapToolAgentManager1Proxy::DESTINATION)]
    well_known_name: String,
    /// Object path of the manager
    #[clap(long, default_value = MapToolAgentManager1Proxy::PATH)]
    object_path: String,
}

#[derive(Debug, Args)]
pub(crate) struct Serve {
    #[clap(flatten)]
    manager: ManagerAddress,
}

impl Serve {
    fn run(self) -> Result<()> {
        let rt = tokio::runtime::Builder::new_current_thread()
            .enable_io()
            .enable_time()
            .build()
            .into_diagnostic()?;
        rt.block_on(crate::serve::serve(
            self.manager.bus.bus_address.as_deref(),
            &self.manager.well_known_name,
            &self.manager.object_path,
        ))
        .into_diagnostic()
    }
}

#[derive(Debug, Args)]
pub(crate) struct Connect {
    #[clap(flatten)]
    manager: ManagerAddress,
    name: String,
    #[clap(subcommand)]
    address: maptool_agent_api::OwnedServerAddress<String, String, String>,
}

impl Connect {
    fn run(self) -> Result<()> {
        let password =
            rpassword::read_password_from_tty(Some("Enter server password: ")).into_diagnostic()?;
        let confirmation =
            rpassword::read_password_from_tty(Some("Confirm password: ")).into_diagnostic()?;
        miette::ensure!(password == confirmation, "Passwords not equal");
        let rt = tokio::runtime::Builder::new_current_thread()
            .enable_io()
            .enable_time()
            .build()
            .into_diagnostic()?;
        let object_path = rt
            .block_on(crate::connect::connect(
                self.manager.bus.bus_address.as_deref(),
                &self.manager.well_known_name,
                &self.manager.object_path,
                self.address,
                &self.name,
                password.as_ref(),
            ))
            .into_diagnostic()?;
        println!("{}", object_path.as_str());
        Ok(())
    }
}

#[derive(Debug, Args)]
pub(crate) struct ConnectionAddress {
    #[clap(flatten)]
    bus: Bus,
    /// Well-known name of the connection
    #[clap(long, default_value = MapToolAgentConnection1Proxy::DESTINATION)]
    well_known_name: String,
    /// Object path of the connection
    #[clap(long)]
    object_path: String,
}

#[derive(Debug, Args)]
pub(crate) struct Log {
    #[clap(flatten)]
    connection: ConnectionAddress,
}

impl Log {
    fn run(self) -> Result<()> {
        let rt = tokio::runtime::Builder::new_current_thread()
            .enable_io()
            .enable_time()
            .build()
            .into_diagnostic()?;
        rt.block_on(crate::log::log(
            self.connection.bus.bus_address.as_deref(),
            &self.connection.well_known_name,
            &self.connection.object_path,
        ))
        .into_diagnostic()
        .into()
    }
}

#[derive(Debug, Args)]
pub(crate) struct Disconnect {
    #[clap(flatten)]
    connection: ConnectionAddress,
}

impl Disconnect {
    fn run(self) -> Result<()> {
        let rt = tokio::runtime::Builder::new_current_thread()
            .enable_io()
            .enable_time()
            .build()
            .into_diagnostic()?;
        rt.block_on(crate::disconnect::disconnect(
            self.connection.bus.bus_address.as_deref(),
            &self.connection.well_known_name,
            &self.connection.object_path,
        ))
        .into_diagnostic()
        .into()
    }
}

#[derive(Debug, Args)]
pub(crate) struct SendRawMessage {
    #[clap(flatten)]
    connection: ConnectionAddress,
}

impl SendRawMessage {
    fn run(self) -> Result<()> {
        use std::io::Read;
        let rt = tokio::runtime::Builder::new_current_thread()
            .enable_io()
            .enable_time()
            .build()
            .into_diagnostic()?;
        let mut input = Vec::new();
        std::io::stdin().read_to_end(&mut input).into_diagnostic()?;
        rt.block_on(crate::send::send_raw_message(
            self.connection.bus.bus_address.as_deref(),
            &self.connection.well_known_name,
            &self.connection.object_path,
            input,
        ))
        .into_diagnostic()
        .into()
    }
}

#[derive(Debug, Args)]
pub(crate) struct Shutdown {
    #[clap(flatten)]
    manager: ManagerAddress,
}

impl Shutdown {
    fn run(self) -> Result<()> {
        let rt = tokio::runtime::Builder::new_current_thread()
            .enable_io()
            .enable_time()
            .build()
            .into_diagnostic()?;
        rt.block_on(crate::shutdown::shutdown(
            self.manager.bus.bus_address.as_deref(),
            &self.manager.well_known_name,
            &self.manager.object_path,
        ))
        .into_diagnostic()
        .into()
    }
}

#[derive(Debug, Parser)]
pub(crate) enum Cli {
    /// Installs links, bash completions and systemd unit.
    ///
    /// If the XDG bindir does not exist but ~/bin does
    /// then links will be installed in there,
    /// but if no user bindir exists install fails
    /// rather than creating the directory,
    /// so as to avoid surprise that they aren't found until restart.
    ///
    /// The systemd unit is installed into the user config path,
    /// but is not enabled or started.
    #[cfg(feature = "install")]
    Install(Install),
    // TODO: Type=bus should mean we don't need to daemonize,
    // and the systemd unit should be written without daemonizing
    // but it may be convenient for scripting to be less opinionated.
    /// Start the agent and bind to a well-known name on D-Bus.
    Serve(Serve),
    /// Instruct the agent to connect to a server
    Connect(Connect),
    /// Poll connection for server messages
    Log(Log),
    /// Send a message to the server
    SendRawMessage(SendRawMessage),
    // TODO: Should also shut down server if last connection
    /// Instruct the agent to disconnect from the server
    Disconnect(Disconnect),
    /// Instruct the manager to stop accepting new connections
    Shutdown(Shutdown),
}

impl Cli {
    fn run(self) -> Result<()> {
        match self {
            Cli::Install(install) => install.run(),
            Cli::Serve(serve) => serve.run(),
            Cli::Shutdown(shutdown) => shutdown.run(),
            Cli::Connect(connect) => connect.run(),
            Cli::Log(log) => log.run(),
            Cli::SendRawMessage(send) => send.run(),
            Cli::Disconnect(disconnect) => disconnect.run(),
        }
    }
}

#[derive(Debug, Parser)]
#[clap(setting = clap::AppSettings::Multicall)]
pub(crate) enum Applet {
    #[clap(name = env!("CARGO_PKG_NAME"), subcommand)]
    Cli(Cli),
    #[clap(name = concat!(env!("CARGO_PKG_NAME"), "d"))]
    Daemon(Serve),
    #[clap(external_subcommand)]
    Unrecognised(Vec<OsString>),
}

impl Applet {
    pub(crate) fn run(self) -> Result<()> {
        match self {
            Applet::Cli(cli) => cli.run(),
            Applet::Daemon(serve) => serve.run(),
            Applet::Unrecognised(args) => Cli::parse_from(args).run(),
        }
    }
}
