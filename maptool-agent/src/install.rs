use std::{
    collections::BTreeSet,
    env, fs,
    io::{Error as IoError, Write},
    path::{Path, PathBuf},
};

use clap::IntoApp;
use miette::Diagnostic;
use thiserror::Error;

use maptool_agent_api::zbus::ProxyDefault;

#[derive(Debug, Diagnostic, Error)]
pub(crate) enum FindBashCompletionsDirError {
    #[error("Unable to find home directory")]
    UnfoundHomeDirectory,
    #[error("Unable to create completions directory")]
    DirUncreatable(#[from] IoError),
}

fn find_bash_completions_dir() -> Result<PathBuf, FindBashCompletionsDirError> {
    use FindBashCompletionsDirError::*;
    match env::var_os("BASH_COMPLETION_USER_DIR") {
        Some(dir) if !dir.is_empty() => {
            let path = PathBuf::from(dir);
            if path.is_absolute() {
                return Ok(path);
            }
            log::warn!("Provided BASH_COMPLETION_USER_DIR is not an absolute path");
        }
        _ => (),
    }
    let mut dir = dirs::data_dir().ok_or(UnfoundHomeDirectory)?;
    dir.push("bash-completion");
    dir.push("completions");
    fs::create_dir_all(&dir)?;
    Ok(dir)
}

#[derive(Debug, Diagnostic, Error)]
pub(crate) enum InstallBashCompletionsError {
    #[error("Failed to find Bash completions directory")]
    FindBashCompletionsDirFailed(#[from] FindBashCompletionsDirError),
    #[error("Failed to open completion file")]
    OpenCompletionFailed(#[source] IoError),
}

/// Generate bash completions and install them to $BASH_COMPLETION_USER_DIR,
/// $XDG_DATA_HOME/bash-completion or ~/.local/share/bash-completion
pub(crate) fn install_bash_completions(clobber: bool) -> Result<(), InstallBashCompletionsError> {
    use InstallBashCompletionsError::*;
    let mut path = find_bash_completions_dir()?;
    for applet in crate::cli::Applet::into_app().get_subcommands_mut() {
        let name = applet.get_name().to_owned();
        path.push(&name);
        path.set_extension("sh");
        let mut f = std::fs::OpenOptions::new()
            .write(true)
            .truncate(clobber)
            .create(clobber)
            .create_new(!clobber)
            .open(&path)
            .map_err(OpenCompletionFailed)?;
        clap_complete::generate(clap_complete::Shell::Bash, applet, name, &mut f);
        path.pop();
    }
    Ok(())
}

#[derive(Debug, Diagnostic, Error)]
pub(crate) enum FindBindirError {
    #[error("PATH is not set")]
    PathNotSet,
    #[error("Unable to find home directory")]
    UnfoundHomeDirectory,
    #[error("No user bindir in XDG path or ~/bin set in PATH")]
    NoExistingBindir,
}

fn find_bindir() -> Result<PathBuf, FindBindirError> {
    use FindBindirError::*;
    let paths: BTreeSet<PathBuf> =
        env::split_paths(&env::var_os("PATH").ok_or(PathNotSet)?).collect();
    let xdg_bindir = dirs::executable_dir().ok_or(UnfoundHomeDirectory)?;
    if paths.contains(&xdg_bindir) {
        return Ok(xdg_bindir);
    }
    let mut legacy_bindir = dirs::home_dir().ok_or(UnfoundHomeDirectory)?;
    legacy_bindir.push("bin");
    if paths.contains(&legacy_bindir) {
        return Ok(legacy_bindir);
    }
    Err(NoExistingBindir)
}

#[derive(Debug, Diagnostic, Error)]
pub(crate) enum InstallLinksError {
    #[error("Failed to find process executable")]
    FindExecutableFailed(#[source] IoError),
    #[error("Failed to find user bindir")]
    FindBindirFailed(#[from] FindBindirError),
    #[error("Failed to link executable")]
    LinkFailed(#[source] IoError),
}

/// Install (sym)links to the executable in the user bin dir,
/// returning which bin dir was usable, preferring XDG
pub(crate) fn install_links(symlink: bool, clobber: bool) -> Result<PathBuf, InstallLinksError> {
    use InstallLinksError::*;
    let exe = std::env::current_exe().map_err(FindExecutableFailed)?;
    let mut path = find_bindir()?;
    for applet in crate::cli::Applet::into_app().get_subcommands_mut() {
        path.push(applet.get_name());
        let res = if symlink {
            #[cfg(unix)]
            use std::os::unix::fs::symlink;
            #[cfg(windows)]
            use std::os::windows::fs::symlink_file as symlink;
            symlink(&exe, &path)
        } else {
            fs::hard_link(&exe, &path)
        };
        match res {
            Err(e) if e.kind() == std::io::ErrorKind::AlreadyExists && clobber => (),
            Err(e) => {
                return Err(LinkFailed(e));
            }
            Ok(()) => (),
        }
        path.pop();
    }
    Ok(path)
}

#[derive(Debug, Diagnostic, Error)]
pub(crate) enum InstallSystemdUnitError {
    #[error("Failed to encode link path")]
    EncodePathFailed,
    #[error("Unable to find home directory")]
    UnfoundHomeDirectory,
    #[error("Failed to make systemd user unit directory")]
    MakeUserUnitDirFailed(#[source] IoError),
    #[error("Failed to open unit file")]
    OpenUnitFailed(#[source] IoError),
    #[error("Failed to write unit file")]
    WriteUnitFailed(#[source] IoError),
    #[error("Failed to make user dbus service directory")]
    MakeUserDBusServiceDirFailed(#[source] IoError),
    #[error("Failed to open bus activation service file")]
    OpenDBusServiceFailed(#[source] IoError),
    #[error("Failed to write bus activation service file")]
    WriteDBusServiceFailed(#[source] IoError),
}

/// Install systemd unit and D-Bus activation config
pub(crate) fn install_service(
    clobber: bool,
    links_dir: impl AsRef<Path>,
) -> Result<(), InstallSystemdUnitError> {
    use InstallSystemdUnitError::*;
    let links_dir_str = links_dir.as_ref().to_str().ok_or(EncodePathFailed)?;

    let bus_name = maptool_agent_api::MapToolAgentManager1Proxy::DESTINATION;
    let systemd_service = format!("dbus-{}.service", &bus_name);
    let mut path = dirs::config_dir().ok_or(UnfoundHomeDirectory)?;
    path.push("systemd");
    path.push("user");
    fs::create_dir_all(&path).map_err(MakeUserUnitDirFailed)?;
    path.push(&systemd_service);

    let mut f = std::fs::OpenOptions::new()
        .write(true)
        .truncate(clobber)
        .create(clobber)
        .create_new(!clobber)
        .open(&path)
        .map_err(OpenUnitFailed)?;
    indoc::writedoc!(
        &mut f,
        "
        [Unit]
        Description={description}
        [Service]
        Type=dbus
        BusName={bus_name}
        ExecStart={links_dir}/{pkg_name}d 
        ",
        bus_name = &bus_name,
        description = env!("CARGO_PKG_DESCRIPTION"),
        links_dir = links_dir_str,
        pkg_name = env!("CARGO_PKG_NAME"),
    )
    .map_err(WriteUnitFailed)?;

    let dbus_service = format!("{}.service", &bus_name);
    let mut path = dirs::data_dir().ok_or(UnfoundHomeDirectory)?;
    path.push("dbus-1");
    path.push("services");
    fs::create_dir_all(&path).map_err(MakeUserDBusServiceDirFailed)?;
    path.push(&dbus_service);
    let mut f = std::fs::OpenOptions::new()
        .write(true)
        .truncate(clobber)
        .create(clobber)
        .create_new(!clobber)
        .open(&path)
        .map_err(OpenDBusServiceFailed)?;
    indoc::writedoc!(
        &mut f,
        "
        [D-BUS Service]
        Name={bus_name}
        Exec={links_dir}/{pkg_name}d
        SystemdService={systemd_service}
        ",
        bus_name = &bus_name,
        systemd_service = &systemd_service,
        links_dir = links_dir_str,
        pkg_name = env!("CARGO_PKG_NAME"),
    )
    .map_err(WriteDBusServiceFailed)?;
    Ok(())
}
