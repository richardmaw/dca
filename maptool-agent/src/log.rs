use futures::stream::StreamExt;
use miette::Diagnostic;
use thiserror::Error;

use maptool_agent_api::{
    zbus::{ConnectionBuilder, Error as ZBusError},
    MapToolAgentSession1Proxy, MessageTaskReadError, SubscribeError,
};

#[derive(Debug, Diagnostic, Error)]
pub(crate) enum LogError {
    #[error("Failed to connect to bus")]
    DBusConnectionFailure(#[source] ZBusError),
    #[error("Failed to set object destination")]
    SetDestinationFailure(#[source] ZBusError),
    #[error("Failed to set object path")]
    SetPathFailure(#[source] ZBusError),
    #[error("Failed to create session proxy")]
    CreateSessionProxyFailure(#[source] ZBusError),
    #[error("Failed to request name owner change stream")]
    RequestOwnerChangeStreamFailure(#[source] ZBusError),
    #[error("Failed to request raw message stream")]
    RequestRawMessagesStreamFailure(#[source] ZBusError),
    #[error("Failed to request read error stream")]
    RequestReadErrorStreamFailure(#[source] ZBusError),
    #[error("Failed to subscribe to session messages")]
    MessageSubscribeFailure(#[source] SubscribeError),
    #[error("Signal stream ended unexpectedly")]
    SignalStreamUnexpectedEnd,
    #[error("Reading Raw Message content failed")]
    RawMessageReadContentFailed(#[source] ZBusError),
    #[error("Reading Raw Message content failed")]
    ReadErrorReadContentFailed(#[source] ZBusError),
    #[error("Reading session messages failed")]
    ReadError(#[source] MessageTaskReadError),
    #[error("Service disconnected without reported error")]
    ServiceDisconnectedUnexpectedly,
}
pub(crate) async fn log(
    bus_address: Option<&str>,
    well_known_name: &str,
    object_path: &str,
) -> Result<(), LogError> {
    use LogError::*;
    let conn = match bus_address {
        Some(addr) => {
            log::debug!("Connecting to bus {}", &addr);
            ConnectionBuilder::address(addr)
        }
        None => {
            log::debug!("Connecting to session bus");
            ConnectionBuilder::session()
        }
    }
    .map_err(DBusConnectionFailure)?
    .internal_executor(false)
    .build()
    .await
    .map_err(DBusConnectionFailure)?;

    let mut log = Box::pin(async {
        let session = MapToolAgentSession1Proxy::builder(&conn)
            .destination(well_known_name)
            .map_err(SetDestinationFailure)?
            .path(object_path)
            .map_err(SetPathFailure)?
            .build()
            .await
            .map_err(CreateSessionProxyFailure)?;

        let mut name_owner_changeds = session
            .receive_owner_changed()
            .await
            .map_err(RequestOwnerChangeStreamFailure)?;
        let mut raw_messages = session
            .receive_raw_message()
            .await
            .map_err(RequestRawMessagesStreamFailure)?;
        let mut read_errors = session
            .receive_read_error()
            .await
            .map_err(RequestReadErrorStreamFailure)?;
        session.subscribe().await.map_err(MessageSubscribeFailure)?;

        loop {
            tokio::select! {
                msg = raw_messages.next() => {
                    let msg = msg.ok_or(SignalStreamUnexpectedEnd)?;
                    let args = msg.args().map_err(RawMessageReadContentFailed)?;
                    let msg = args.message;
                    println!("{}", &String::from_utf8_lossy(&msg));
                }
                msg = read_errors.next() => {
                    let msg = msg.ok_or(SignalStreamUnexpectedEnd)?;
                    let args = msg.args().map_err(ReadErrorReadContentFailed)?;
                    break Err(ReadError(args.error))
                }
                change = name_owner_changeds.next() => {
                    let change = change.ok_or(SignalStreamUnexpectedEnd)?;
                    match change {
                        None => break Err(ServiceDisconnectedUnexpectedly),
                        Some(name) => unimplemented!("Unhandled name change {:?}", name),
                    }
                }
            }
        }
    });

    loop {
        tokio::select! {
            v = &mut log => break v,
            _ = conn.executor().tick() => (),
        }
    }
}
