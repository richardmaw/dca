use clap::Parser;
use miette::Result;

mod cli;
mod connect;
mod disconnect;
#[cfg(feature = "install")]
mod install;
mod log;
mod send;
mod serve;
mod shutdown;

fn main() -> Result<()> {
    env_logger::init();
    let applet = cli::Applet::parse();
    applet.run()?;
    Ok(())
}
