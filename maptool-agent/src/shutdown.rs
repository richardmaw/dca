use miette::Diagnostic;
use thiserror::Error;

use maptool_agent_api::{
    zbus::{fdo, ConnectionBuilder, Error as ZBusError},
    MapToolAgentManager1Proxy,
};

#[derive(Debug, Diagnostic, Error)]
pub(crate) enum ShutdownError {
    #[error("Failed to connect to bus")]
    DBusConnectionFailure(#[source] ZBusError),
    #[error("Failed to set object destination")]
    SetDestinationFailure(#[source] ZBusError),
    #[error("Failed to set object path")]
    SetPathFailure(#[source] ZBusError),
    #[error("Failed to create session proxy")]
    CreateManagerProxyFailure(#[source] ZBusError),
    #[error("Failed to request disconnect")]
    ShutdownFailed(#[source] fdo::Error),
}
pub(crate) async fn shutdown(
    bus_address: Option<&str>,
    well_known_name: &str,
    object_path: &str,
) -> Result<(), ShutdownError> {
    use ShutdownError::*;
    let conn = match bus_address {
        Some(addr) => {
            log::debug!("Connecting to bus {}", &addr);
            ConnectionBuilder::address(addr)
        }
        None => {
            log::debug!("Connecting to session bus");
            ConnectionBuilder::session()
        }
    }
    .map_err(DBusConnectionFailure)?
    .internal_executor(false)
    .build()
    .await
    .map_err(DBusConnectionFailure)?;

    let mut shutdown = Box::pin(async {
        let manager = MapToolAgentManager1Proxy::builder(&conn)
            .destination(well_known_name)
            .map_err(SetDestinationFailure)?
            .path(object_path)
            .map_err(SetPathFailure)?
            .build()
            .await
            .map_err(CreateManagerProxyFailure)?;

        manager.shutdown().await.map_err(ShutdownFailed)
    });

    loop {
        tokio::select! {
            v = &mut shutdown => break v,
            _ = conn.executor().tick() => (),
        }
    }
}
