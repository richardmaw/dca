use miette::Diagnostic;
use thiserror::Error;

use maptool_agent_api::{
    zbus::{zvariant::OwnedObjectPath, ConnectionBuilder, Error as ZBusError},
    MapToolAgentManager1Proxy, OwnedServerAddress,
};

#[derive(Debug, Diagnostic, Error)]
pub(crate) enum ConnectError {
    #[error("Failed to connect to bus")]
    DBusConnectionFailure(#[source] ZBusError),
    #[error("Failed to set object destination")]
    SetDestinationFailure(#[source] ZBusError),
    #[error("Failed to set object path")]
    SetPathFailure(#[source] ZBusError),
    #[error("Failed to create agent manager proxy")]
    CreateManagerProxyFailure(#[source] ZBusError),
    #[error("Failed to connect with direct address")]
    DirectConnectFailure(#[source] ZBusError),
    #[error("Failed to connect with webrtc address")]
    WebRtcConnectFailure(#[source] ZBusError),
    #[error("Failed to handshake with connected server")]
    HandshakeFailure(#[source] ZBusError),
}
pub(crate) async fn connect(
    bus_address: Option<&str>,
    well_known_name: &str,
    object_path: &str,
    address: OwnedServerAddress<String, String, String>,
    name: &str,
    password: &[u8],
) -> Result<OwnedObjectPath, ConnectError> {
    use ConnectError::*;
    let conn = match bus_address {
        Some(addr) => {
            log::debug!("Connecting to bus {}", &addr);
            ConnectionBuilder::address(addr)
        }
        None => {
            log::debug!("Connecting to session bus");
            ConnectionBuilder::session()
        }
    }
    .map_err(DBusConnectionFailure)?
    .internal_executor(false)
    .build()
    .await
    .map_err(DBusConnectionFailure)?;

    let mut connect = Box::pin(async {
        let manager = MapToolAgentManager1Proxy::builder(&conn)
            .destination(well_known_name)
            .map_err(SetDestinationFailure)?
            .path(object_path)
            .map_err(SetPathFailure)?
            .build()
            .await
            .map_err(CreateManagerProxyFailure)?;

        log::debug!("Connecting with address {:?}", &address);
        let connection = match address {
            OwnedServerAddress::Direct(address) => manager
                .connect_direct(address)
                .await
                .map_err(DirectConnectFailure)?,
            OwnedServerAddress::WebRtc(address) => manager
                .connect_webrtc(address)
                .await
                .map_err(WebRtcConnectFailure)?,
        };

        // TODO: Poll connection.pin for PIN notifications to report.

        log::debug!(
            "Handshaking with name {} and password: {}",
            &name,
            String::from_utf8_lossy(&password)
        );
        let session = connection
            .handshake(
                name,
                &Some(password.to_vec()).into(),
                &None.into(),
                &None.into(),
            )
            .await
            .map_err(HandshakeFailure)?;

        Ok(OwnedObjectPath::from(session.path().to_owned()))
    });

    loop {
        tokio::select! {
            v = &mut connect => break v,
            _ = conn.executor().tick() => (),
        }
    }
}
