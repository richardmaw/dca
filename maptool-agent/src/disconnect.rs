use miette::Diagnostic;
use thiserror::Error;

use maptool_agent_api::{
    zbus::{fdo, ConnectionBuilder, Error as ZBusError},
    MapToolAgentSession1Proxy,
};

#[derive(Debug, Diagnostic, Error)]
pub(crate) enum DisconnectError {
    #[error("Failed to connect to bus")]
    DBusConnectionFailure(#[source] ZBusError),
    #[error("Failed to set object destination")]
    SetDestinationFailure(#[source] ZBusError),
    #[error("Failed to set object path")]
    SetPathFailure(#[source] ZBusError),
    #[error("Failed to create session proxy")]
    CreateSessionProxyFailure(#[source] ZBusError),
    #[error("Failed to request disconnect")]
    DisconnectFailed(#[source] fdo::Error),
}
pub(crate) async fn disconnect(
    bus_address: Option<&str>,
    well_known_name: &str,
    object_path: &str,
) -> Result<(), DisconnectError> {
    use DisconnectError::*;
    let conn = match bus_address {
        Some(addr) => {
            log::debug!("Connecting to bus {}", &addr);
            ConnectionBuilder::address(addr)
        }
        None => {
            log::debug!("Connecting to session bus");
            ConnectionBuilder::session()
        }
    }
    .map_err(DBusConnectionFailure)?
    .internal_executor(false)
    .build()
    .await
    .map_err(DBusConnectionFailure)?;

    let mut disconnect = Box::pin(async {
        let session = MapToolAgentSession1Proxy::builder(&conn)
            .destination(well_known_name)
            .map_err(SetDestinationFailure)?
            .path(object_path)
            .map_err(SetPathFailure)?
            .build()
            .await
            .map_err(CreateSessionProxyFailure)?;

        session.disconnect().await.map_err(DisconnectFailed)
    });

    loop {
        tokio::select! {
            v = &mut disconnect => break v,
            _ = conn.executor().tick() => (),
        }
    }
}
