use std::{
    convert::{TryFrom, TryInto},
    sync::Arc,
};

use async_tungstenite::tungstenite::{
    error::Error as TungsteniteError, http::uri::Uri, protocol::Message,
};
use bytes::Bytes;
use futures::{
    channel::oneshot, future::Fuse, AsyncReadExt, AsyncWriteExt, FutureExt, SinkExt, StreamExt,
};
use serde::{Deserialize, Serialize};
use serde_json::error::Error as SerdeJsonError;
use thiserror::Error;
use tokio::sync::Mutex;
use tokio_util::compat::{Compat, FuturesAsyncReadCompatExt, TokioAsyncReadCompatExt};
use typed_builder::TypedBuilder;
use url::Url;
use webrtc::{
    api::{interceptor_registry, media_engine::MediaEngine},
    data_channel::data_channel_init::RTCDataChannelInit,
    error::Error as WebRtcError,
    ice_transport::{ice_candidate::RTCIceCandidateInit, ice_server::RTCIceServer},
    interceptor::registry::Registry as InterceptorRegistry,
    peer_connection::{
        configuration::RTCConfiguration,
        peer_connection_state::RTCPeerConnectionState,
        sdp::{
            sdp_type::RTCSdpType as WebRtcRsRTCSdpType,
            session_description::RTCSessionDescription as WebRtcRsRTCSessionDescription,
        },
        RTCPeerConnection,
    },
};

const STREAM_BUF_SIZE: usize = 4096;

#[derive(Clone, Copy, Debug, Deserialize, Serialize)]
#[serde(rename_all = "SCREAMING_SNAKE_CASE")]
pub(crate) enum RTCSdpType {
    Offer,
    PrAnswer,
    Answer,
    Rollback,
}

#[derive(Debug, Error)]
#[cfg_attr(feature = "miette", derive(miette::Diagnostic))]
pub(crate) enum SessionDescriptionConversionError {
    #[error("Session Description type is unspecified")]
    TypeUnspecified,
}
impl Into<WebRtcRsRTCSdpType> for RTCSdpType {
    fn into(self) -> WebRtcRsRTCSdpType {
        match self {
            Self::Offer => WebRtcRsRTCSdpType::Offer,
            Self::PrAnswer => WebRtcRsRTCSdpType::Pranswer,
            Self::Answer => WebRtcRsRTCSdpType::Answer,
            Self::Rollback => WebRtcRsRTCSdpType::Rollback,
        }
    }
}
impl TryFrom<WebRtcRsRTCSdpType> for RTCSdpType {
    type Error = SessionDescriptionConversionError;
    fn try_from(sdp_type: WebRtcRsRTCSdpType) -> Result<Self, Self::Error> {
        match sdp_type {
            WebRtcRsRTCSdpType::Unspecified => {
                Err(SessionDescriptionConversionError::TypeUnspecified)
            }
            WebRtcRsRTCSdpType::Offer => Ok(Self::Offer),
            WebRtcRsRTCSdpType::Pranswer => Ok(Self::PrAnswer),
            WebRtcRsRTCSdpType::Answer => Ok(Self::Answer),
            WebRtcRsRTCSdpType::Rollback => Ok(Self::Rollback),
        }
    }
}

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub(crate) struct RTCSessionDescription {
    sdp_type: RTCSdpType,
    sdp: String,
}

impl Into<WebRtcRsRTCSessionDescription> for RTCSessionDescription {
    fn into(self) -> WebRtcRsRTCSessionDescription {
        let mut sd = WebRtcRsRTCSessionDescription::default();
        sd.sdp_type = self.sdp_type.into();
        sd.sdp = self.sdp;
        sd
    }
}
impl TryFrom<WebRtcRsRTCSessionDescription> for RTCSessionDescription {
    type Error = SessionDescriptionConversionError;
    fn try_from(
        sd: WebRtcRsRTCSessionDescription,
    ) -> Result<Self, SessionDescriptionConversionError> {
        Ok(Self {
            sdp_type: sd.sdp_type.try_into()?,
            sdp: sd.sdp,
        })
    }
}

#[derive(Debug, Default, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub(crate) struct RTCIceCandidate {
    // Strictly MapTool could omit this if init'd with null,
    // but we can't fix that so let that fail at parse time
    sdp: String,
    #[serde(default, skip_serializing_if = "Option::is_none")]
    sdp_mid: Option<String>,
    #[serde(default)]
    sdp_mline_index: u16,
    #[serde(default, skip_serializing_if = "Option::is_none")]
    server_url: Option<String>,
}

impl Into<RTCIceCandidateInit> for RTCIceCandidate {
    fn into(self) -> RTCIceCandidateInit {
        RTCIceCandidateInit {
            candidate: self.sdp,
            sdp_mid: self.sdp_mid.unwrap_or_default(),
            sdp_mline_index: self.sdp_mline_index,
            username_fragment: self.server_url.map_or("".to_owned(), |s| {
                Url::try_from(s.as_str())
                    .expect("server_url should be URL")
                    .username()
                    .to_owned()
            }),
        }
    }
}

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "lowercase", tag = "type")]
pub(crate) enum SignalingMsgType {
    Login { success: bool },
    Offer { offer: RTCSessionDescription },
    Candidate { candidate: RTCIceCandidate },
    Answer { answer: RTCSessionDescription },
}

#[derive(Debug, Deserialize, Serialize)]
pub struct ClientSignalingMsg {
    #[serde(flatten)]
    pub(crate) inner: SignalingMsgType,
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub(crate) source: Option<String>,
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub(crate) destination: Option<String>,
}

impl TryFrom<ClientSignalingMsg> for Message {
    type Error = SerdeJsonError;
    fn try_from(m: ClientSignalingMsg) -> Result<Self, Self::Error> {
        Ok(Self::Text(serde_json::to_string(&m)?))
    }
}

pub(crate) const REGISTRY_WEBSOCKET_URL: &'static str = "ws://webrtc1.rptools.net:8080";
pub(crate) const DEFAULT_ICE_SERVER_URLS: [&'static str; 5] = [
    "stun:stun.l.google.com:19302",
    "stun:stun1.l.google.com:19302",
    "stun:stun2.l.google.com:19302",
    "stun:stun3.l.google.com:19302",
    "stun:stun4.l.google.com:19302",
];

#[derive(Debug, TypedBuilder)]
pub struct Signaller<'a, 'b> {
    // TODO: Would an implementation where you pass the WebSocket help browser?
    /// Registry server to connect to and send signalling via.
    #[builder(
        default_code = "Uri::from_static(REGISTRY_WEBSOCKET_URL)",
        setter(into)
    )]
    registry: Uri,
    /// Per-server unique ID. Used to route signalling messages.
    /// Conventionally the player name.
    #[builder(setter(into))]
    id: &'a str,
    /// Name of server to connect to.
    /// Must match name used by server.
    #[builder(setter(into))]
    server_name: &'b str,
}

#[derive(Debug, Error)]
#[cfg_attr(feature = "miette", derive(miette::Diagnostic))]
pub enum ConnectError {
    #[error("Failed to connect to registry")]
    RegistryConnectFailed(#[source] futures_io::Error),
    #[error("Failed to serialize registry login message")]
    SerializeLoginMsgFailed(#[source] SerdeJsonError),
    #[error("Failed to send registry login response")]
    SendLoginMsgFailed(#[source] TungsteniteError),
    #[error("Failed to receive registry login response")]
    RecvLoginResponseFailed(#[source] TungsteniteError),
    #[error("Registry hung up in response to login")]
    RecvLoginResponseHangup,
    #[error("Failed to receive registry login response")]
    RecvLoginResponseUnexpectedKind(Message),
    #[error("Failed to deserialize registry login response")]
    DeserializeLoginResponseFailed(#[source] SerdeJsonError),
    #[error("Failed to receive registry login response")]
    LoginResponseUnexpectedType(ClientSignalingMsg),
    #[error("Login responded with failure")]
    LoginFailed(ClientSignalingMsg),
    #[error("Failed to negotiate WebSocket connection")]
    WebSocketNegotiateFailed(#[source] TungsteniteError),
    #[error("Failed to register default codecs")]
    RegisterWebRTCMediaEngineDefaultCodecsFailed(#[source] WebRtcError),
    #[error("Failed to initialize interceptor registry")]
    InitializeWebRTCInterceptorRegistryFailed(#[source] WebRtcError),
    #[error("Failed to create data channel")]
    CreateDataChannelFailed(#[source] WebRtcError),
    #[error("Failed to create sdp offer")]
    CreateOfferFailed(#[source] WebRtcError),
    #[error("Failed to set local sdp description")]
    SetLocalDescriptionFailed(#[source] WebRtcError),
    #[error("Failed to create WebRTC Peer Connection")]
    CreatePeerConnectionFailed(#[source] WebRtcError),
    #[error("Failed to serialize offer message")]
    SerializeOfferMsgFailed(#[source] SerdeJsonError),
    #[error("Failed to send registry login response")]
    SendOfferMsgFailed(#[source] TungsteniteError),
    #[error("Registry hung up in response to signaling")]
    SignalingHangup,
    #[error("Failed to receive registry signaling")]
    SignalingFailed(#[source] TungsteniteError),
    #[error("Signaling responded with unexpected kind")]
    SignalingUnexpectedKind(Message),
    #[error("Failed to deserialize signaling message")]
    DeserializeSignalingMsgFailed(#[source] SerdeJsonError),
    #[error("Signaling responded with unexpected message type")]
    SignalingUnexpectedType(ClientSignalingMsg),
    #[error("Failed to add ICE candidate")]
    AddCandidateFailed(#[source] WebRtcError),
    #[error("Failed to set remote sdp description")]
    SetRemoteDescriptionFailed(#[source] WebRtcError),
    #[error("Failed to negotiate WebRTC connection")]
    WebRtcNegotiationFailed,
}

pub struct SingletonWebRtcDataChannelMessageStream {
    _peer_connection: RTCPeerConnection,
    // TODO: Select on in poll
    _rx_packetizer: tokio::task::JoinHandle<Result<(), WebRtcError>>,
    conn_ep: Compat<futures_ringbuf::Endpoint>,
    // TODO: Select on in poll and teardown after peer_connection
    _fused_failed_rx: Fuse<oneshot::Receiver<()>>,
}

impl tokio::io::AsyncRead for SingletonWebRtcDataChannelMessageStream {
    fn poll_read(
        mut self: core::pin::Pin<&mut Self>,
        cx: &mut core::task::Context<'_>,
        buf: &mut tokio::io::ReadBuf<'_>,
    ) -> core::task::Poll<std::io::Result<()>> {
        core::pin::Pin::new(&mut self.conn_ep).poll_read(cx, buf)
    }
}

impl tokio::io::AsyncWrite for SingletonWebRtcDataChannelMessageStream {
    fn poll_write(
        mut self: core::pin::Pin<&mut Self>,
        cx: &mut core::task::Context<'_>,
        buf: &[u8],
    ) -> core::task::Poll<std::io::Result<usize>> {
        core::pin::Pin::new(&mut self.conn_ep).poll_write(cx, buf)
    }
    fn poll_flush(
        mut self: core::pin::Pin<&mut Self>,
        cx: &mut core::task::Context<'_>,
    ) -> core::task::Poll<std::io::Result<()>> {
        core::pin::Pin::new(&mut self.conn_ep).poll_flush(cx)
    }
    fn poll_shutdown(
        mut self: core::pin::Pin<&mut Self>,
        cx: &mut core::task::Context<'_>,
    ) -> core::task::Poll<std::io::Result<()>> {
        core::pin::Pin::new(&mut self.conn_ep).poll_shutdown(cx)
    }
}

impl<'a, 'b> Signaller<'a, 'b> {
    fn source(&self) -> String {
        format!("{}@{}", self.id, self.server_name)
    }
    // TODO: Split peer connection from signaller connection
    pub async fn connect(
        self,
    ) -> Result<SingletonWebRtcDataChannelMessageStream, ConnectError> {
        use ConnectError::*;
        let port = self.registry.port().map(|p| p.as_u16()).unwrap_or(80);
        let conn = tokio::net::TcpStream::connect((
            self.registry.host().expect("URL must include a host"),
            port,
        ))
        .await
        .map_err(RegistryConnectFailed)?;
        log::debug!("Connected to registry {}", &self.registry);

        let (mut ws, _response) = async_tungstenite::client_async(&self.registry, conn.compat())
            .await
            .map_err(WebSocketNegotiateFailed)?;
        log::debug!("Established WebSocket connection");

        // Send login and await response
        let login_msg = ClientSignalingMsg {
            inner: SignalingMsgType::Login { success: false },
            source: Some(self.source()),
            destination: Default::default(),
        };
        log::debug!("Preparing login msg {:?}", &login_msg);
        let serialized_login = login_msg.try_into().map_err(SerializeLoginMsgFailed)?;
        log::debug!("Sending serialized login msg {:?}", &serialized_login);
        ws.send(serialized_login)
            .await
            .map_err(SendLoginMsgFailed)?;

        loop {
            match ws.next().await {
                Some(Ok(Message::Text(text))) => {
                    log::debug!("Received response: {}", &text);
                    let msg: ClientSignalingMsg =
                        serde_json::from_str(&text).map_err(DeserializeLoginResponseFailed)?;
                    match msg.inner {
                        SignalingMsgType::Login { success: true } => break,
                        SignalingMsgType::Login { success: false } => return Err(LoginFailed(msg)),
                        _ => return Err(LoginResponseUnexpectedType(msg)),
                    }
                }
                Some(Ok(msg)) => return Err(RecvLoginResponseUnexpectedKind(msg)),
                Some(Err(e)) => return Err(RecvLoginResponseFailed(e)),
                None => return Err(RecvLoginResponseHangup),
            }
        }
        log::info!("Successfully logged in");

        let mut m = MediaEngine::default();
        m.register_default_codecs()
            .map_err(RegisterWebRTCMediaEngineDefaultCodecsFailed)?;

        let registry = InterceptorRegistry::new();
        let registry = interceptor_registry::register_default_interceptors(registry, &mut m)
            .await
            .map_err(InitializeWebRTCInterceptorRegistryFailed)?;

        let api = webrtc::api::APIBuilder::new()
            .with_media_engine(m)
            .with_interceptor_registry(registry)
            .build();
        let config = RTCConfiguration {
            ice_servers: vec![RTCIceServer {
                urls: DEFAULT_ICE_SERVER_URLS
                    .iter()
                    .map(ToString::to_string)
                    .collect(),
                ..Default::default()
            }],
            ..Default::default()
        };
        let peer_connection = api
            .new_peer_connection(config)
            .await
            .map_err(CreatePeerConnectionFailed)?;

        let (failed_tx, failed_rx) = oneshot::channel();
        let failed_tx_cell = Arc::new(Mutex::new(Some(failed_tx)));
        peer_connection
            .on_peer_connection_state_change(Box::new(move |s| {
                log::debug!("Peer connection state changed: {:?}", &s);
                let failed_tx_cell = Arc::clone(&failed_tx_cell);
                Box::pin(async move {
                    if s == RTCPeerConnectionState::Failed {
                        // NOTE: Potential deadlock here
                        let failed_tx = failed_tx_cell
                            .lock()
                            .await
                            .take()
                            .expect("Should only be called with Failed once");
                        failed_tx
                            .send(())
                            .expect("failed RX outlives PeerConnection")
                    }
                })
            }))
            .await;

        let data_channel = peer_connection
            .create_data_channel(
                // TODO: Watch out for MapTool changing channel name
                "myDataChannel",
                Some(RTCDataChannelInit {
                    // TODO: Watch out MapTool adding protocol name
                    ordered: Some(true),
                    negotiated: Some(false),
                    ..Default::default()
                }),
            )
            .await
            .map_err(CreateDataChannelFailed)?;
        let (dc_open_tx, dc_open_rx) = oneshot::channel();
        data_channel
            .on_open(Box::new(|| {
                Box::pin(async { dc_open_tx.send(()).expect("dc_open_rx still live") })
            }))
            .await;

        // TODO: Instead of having to work out how to translate Async{Read,Write} into Sink/Stream,
        // use futures_ringbuf::Endpoint to have two ringbufs,
        // allow on_message to write to dc_tx and add a task to read dc_rx,
        // let client end be written/read by proxy method impl
        let (dc_ep, conn_ep) = futures_ringbuf::Endpoint::pair(STREAM_BUF_SIZE, STREAM_BUF_SIZE);
        let conn_ep = conn_ep.compat();
        let (mut dc_rx, dc_tx) = dc_ep.split();
        let dc_tx = Arc::new(Mutex::new(dc_tx));
        data_channel
            .on_message(Box::new(move |msg| {
                let dc_tx = Arc::clone(&dc_tx);
                Box::pin(async move {
                    dc_tx
                        .lock()
                        .await
                        .write_all(msg.data.as_ref())
                        .await
                        .expect("msg stream reader should outlive writer");
                })
            }))
            .await;
        let rx_packetizer = tokio::spawn(async move {
            let mut buf = [0; STREAM_BUF_SIZE];
            loop {
                let len = dc_rx
                    .read(&mut buf[..])
                    .await
                    .expect("RingBuf should always return ok, and return 0 on hangup");
                if len == 0 {
                    break Ok(());
                }
                let subslice = Bytes::copy_from_slice(&buf[..len]);
                data_channel.send(&subslice).await?;
            }
        });

        let offer = peer_connection
            .create_offer(None)
            .await
            .map_err(CreateOfferFailed)?;
        peer_connection
            .set_local_description(offer.clone())
            .await
            .map_err(SetLocalDescriptionFailed)?;

        // TODO: Send offer message
        let offer_msg = ClientSignalingMsg {
            inner: SignalingMsgType::Offer {
                offer: offer.try_into().expect("Offer must be valid"),
            },
            source: Some(self.source()),
            destination: Some(self.server_name.to_owned()),
        };
        log::debug!("Preparing offer msg {:?}", &offer_msg);
        let serialized_offer = offer_msg.try_into().map_err(SerializeOfferMsgFailed)?;
        log::debug!("Sending serialized offer msg {:?}", &serialized_offer);
        ws.send(serialized_offer).await.map_err(SendOfferMsgFailed)?;

        let mut fused_dc_open_rx = dc_open_rx.fuse();
        let mut fused_failed_rx = failed_rx.fuse();
        loop {
            tokio::select! {
                _ = &mut fused_failed_rx => {
                    return Err(WebRtcNegotiationFailed);
                }
                _ = &mut fused_dc_open_rx => {
                    break;
                }
                // TODO: Is this future drop-safe?
                msg = ws.next() => {
                    match msg {
                        Some(Ok(Message::Text(text))) => {
                            log::debug!("Received msg {}", &text);
                            let msg: ClientSignalingMsg =
                                serde_json::from_str(&text).map_err(DeserializeSignalingMsgFailed)?;
                            match msg.inner {
                                SignalingMsgType::Candidate { candidate } => {
                                    peer_connection.add_ice_candidate(candidate.into()).await.map_err(AddCandidateFailed)?;
                                }
                                SignalingMsgType::Answer { answer } => {
                                    peer_connection.set_remote_description(answer.into()).await.map_err(SetRemoteDescriptionFailed)?;
                                }
                                _ => return Err(SignalingUnexpectedType(msg)),
                            }
                        }
                        Some(Ok(msg)) => return Err(SignalingUnexpectedKind(msg)),
                        Some(Err(e)) => return Err(SignalingFailed(e)),
                        None => return Err(SignalingHangup),
                    }
                }
            }
        }

        Ok(SingletonWebRtcDataChannelMessageStream {
            _peer_connection: peer_connection,
            _rx_packetizer: rx_packetizer,
            conn_ep,
            _fused_failed_rx: fused_failed_rx,
        })
    }
}
