use pkcs5::pbes2::{
    EncryptionScheme as Pbes2EncryptionScheme, Parameters as Pbes2Parameters, Pbkdf2Params,
    Pbkdf2Prf,
};

pub(crate) fn pbkdf2_with_hmac_sha1_aes128cbc<'a>(salt: &'a [u8], iv: &'a [u8; 16]) -> Pbes2Parameters<'a> {
    const KEY_ITERATION_KEY_COUNT: u32 = 2000;
    let kdf = Pbkdf2Params {
        salt: salt,
        iteration_count: KEY_ITERATION_KEY_COUNT,
        key_length: None, // TODO should this be 128 bit i.e. 16?
        prf: Pbkdf2Prf::HmacWithSha1,
    }
    .into();
    let encryption = Pbes2EncryptionScheme::Aes128Cbc { iv };
    Pbes2Parameters { kdf, encryption }
}
