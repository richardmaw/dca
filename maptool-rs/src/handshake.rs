use core::{array::TryFromSliceError, convert::TryInto, fmt::Write, future::Future};

use base64ct::{Base64, Encoding};
use bytes::{Bytes, BytesMut};
use futures::{AsyncReadExt, AsyncWriteExt};
use protobuf::{error::ProtobufError, Chars, Message};
use rsa::{
    pkcs1::{self, FromRsaPrivateKey},
    RsaPrivateKey,
};
use thiserror::Error;

use crate::proto;

pub(crate) fn md5sum_pubkey<K>(key: &K) -> String
where
    K: AsRef<[u8]>,
{
    let mut hasher = md5::Context::new();
    hasher.consume("======BeginPublicKey======");
    for chunk in key.as_ref().chunks(60) {
        let mut enc_buf = [0u8; 80];
        let encoded =
            Base64::encode(chunk, &mut enc_buf).expect("60 byte chunks encode to 80 bytes");
        hasher.consume(encoded);
    }
    hasher.consume("======EndPublicKey======");
    let digest = hasher.compute();
    hex::encode(*digest)
}

#[derive(Debug, Error)]
#[cfg_attr(feature = "miette", derive(miette::Diagnostic))]
pub enum ReadMsgError {
    #[error("Failed to read message length")]
    ReadLengthFailed(#[source] futures_io::Error),
    #[error("Failed to read message payload")]
    ReadPayloadFailed(#[source] futures_io::Error),
}

async fn read_msg<C>(mut conn: C, msgbuf: &mut Vec<u8>) -> Result<(), ReadMsgError>
where
    C: futures_io::AsyncRead + Unpin,
{
    let mut lenbuf = [0u8; 4];
    conn.read_exact(&mut lenbuf)
        .await
        .map_err(ReadMsgError::ReadLengthFailed)?;
    let len = u32::from_be_bytes(lenbuf);
    msgbuf.resize(len as usize, 0);
    conn.read_exact(msgbuf.as_mut_slice())
        .await
        .map_err(ReadMsgError::ReadPayloadFailed)?;
    Ok(())
}

#[derive(Debug, Error)]
#[cfg_attr(feature = "miette", derive(miette::Diagnostic))]
pub enum WriteMsgError {
    #[error("Failed to write message length")]
    WriteLengthFailed(#[source] futures_io::Error),
    #[error("Failed to write message payload")]
    WritePayloadFailed(#[source] futures_io::Error),
}

async fn write_msg<C>(mut conn: C, msgbuf: &[u8]) -> Result<(), WriteMsgError>
where
    C: futures_io::AsyncWrite + Unpin,
{
    let msg_len = msgbuf.len() as u32;
    conn.write(&msg_len.to_be_bytes())
        .await
        .map_err(WriteMsgError::WriteLengthFailed)?;
    conn.write(msgbuf)
        .await
        .map_err(WriteMsgError::WritePayloadFailed)?;
    Ok(())
}

pub struct Session<C>
where
    C: futures_io::AsyncRead + futures_io::AsyncWrite,
{
    conn: C,
}

pub struct ReadSession<C>
where
    C: futures_io::AsyncRead + futures_io::AsyncWrite,
{
    rx: futures::io::ReadHalf<C>,
}

pub struct WriteSession<C>
where
    C: futures_io::AsyncRead + futures_io::AsyncWrite,
{
    tx: futures::io::WriteHalf<C>,
}

// TODO: Should instead implement Sink<Bytes> and Stream<Item=BytesMut>
// but also when the protocol's types are available should rewrite to implement
// Sink<Message> and Stream<Result<Message, DeserializeError>> type
impl<C> Session<C>
where
    C: futures_io::AsyncRead + futures_io::AsyncWrite + Unpin,
{
    pub async fn read_msg(&mut self, mut msgbuf: Vec<u8>) -> Result<Vec<u8>, ReadMsgError> {
        msgbuf.clear();
        read_msg(&mut self.conn, &mut msgbuf).await?;
        Ok(msgbuf)
    }

    pub async fn write_msg(&mut self, mut msgbuf: Vec<u8>) -> Result<Vec<u8>, WriteMsgError> {
        write_msg(&mut self.conn, &mut msgbuf).await?;
        Ok(msgbuf)
    }

    pub fn split(self) -> (ReadSession<C>, WriteSession<C>) {
        let (rx, tx) = self.conn.split();
        (ReadSession { rx }, WriteSession { tx })
    }
}

impl<C> ReadSession<C>
where
    C: futures_io::AsyncRead + futures_io::AsyncWrite + Unpin,
{
    pub async fn read_msg(&mut self, mut msgbuf: Vec<u8>) -> Result<Vec<u8>, ReadMsgError> {
        msgbuf.clear();
        read_msg(&mut self.rx, &mut msgbuf).await?;
        Ok(msgbuf)
    }
}

impl<C> WriteSession<C>
where
    C: futures_io::AsyncRead + futures_io::AsyncWrite + Unpin,
{
    pub async fn write_msg(&mut self, mut msgbuf: Vec<u8>) -> Result<Vec<u8>, WriteMsgError> {
        write_msg(&mut self.tx, &mut msgbuf).await?;
        Ok(msgbuf)
    }
}

#[derive(Debug, Error)]
#[cfg_attr(feature = "miette", derive(miette::Diagnostic))]
pub enum HandshakeError<ReportPinError, PrivateKeyError, PasswordError>
where
    ReportPinError: std::error::Error + 'static,
    PrivateKeyError: std::error::Error + 'static,
    PasswordError: std::error::Error + 'static,
{
    #[error("Failed to serialize ClientInitMsg")]
    SerializeClientInitMsgFailed(#[source] ProtobufError),
    #[error("Failed to write ClientInitMsg")]
    WriteClientInitMsgFailed(#[source] WriteMsgError),
    #[error("Failed to read ClientInitMsg response")]
    ReadClientInitMsgResponseFailed(#[source] ReadMsgError),
    #[error("Failed to deserialize ClientInitMsg response")]
    DeserializeClientInitMsgResponseFailed(#[source] ProtobufError),
    #[error("Public Key was requested and none was provided")]
    PublicKeyRequested,
    #[error("Reporting Easy Connect PIN failed")]
    ReportPinFailed(#[source] ReportPinError),
    #[error("Failed to serialize PublicKeyUploadMsg")]
    SerializePublicKeyUploadMsgFailed(#[source] ProtobufError),
    #[error("Failed to write PublicKeyUploadMsg")]
    WritePublicKeyUploadMsgFailed(#[source] WriteMsgError),
    #[error("Failed to read PublicKeyUploadMsg response")]
    ReadPublicKeyUploadMsgResponseFailed(#[source] ReadMsgError),
    #[error("Failed to deserialize PublicKeyUploadMsg response")]
    DeserializePublicKeyUploadMsgResponseFailed(#[source] ProtobufError),
    #[error("Did not receive expected PublicKeyAdded response")]
    PublicKeyUploadMsgResponseUnexpected(proto::handshake::HandshakeMsg),
    #[error("PublicKeyAdded response did not match sent key")]
    PublicKeyUploadMsgResponseKeyMismatch { sent: Chars, received: Chars },
    #[error("Did not receive expected ClientInitMsg response")]
    ClientInitMsgResponseUnexpected(proto::handshake::HandshakeMsg),
    #[error("Password was requested and none was provided")]
    PasswordRequested,
    #[error("UseAuthMsg IV is of incorrect length")]
    WrongIvLength(#[source] TryFromSliceError),
    #[error("Get Password function failed")]
    GetPasswordFailed(#[source] PasswordError),
    #[error("Get Private Key function failed")]
    GetPrivateKeyFailed(#[source] PrivateKeyError),
    #[error("Deserialize Private Key from bytes failed")]
    DeserializePrivateKeyFailed(#[source] pkcs1::Error),
    #[error("Password Challenge decryption failed: {0}")]
    PasswordDecryptFailed(String),
    #[error("Password Challenge encryption failed: {0}")]
    PasswordEncryptFailed(String),
    #[error("Key Challenge decryption failed")]
    KeyDecryptFailed(#[source] rsa::errors::Error),
    #[error("Challenge decryption failed")]
    DecryptedChallengeUnexpected(String),
    #[error("Failed to serialize ClientAuthMsg")]
    SerializeClientAuthMsgFailed(#[source] ProtobufError),
    #[error("Failed to write ClientAuthMsg")]
    WriteClientAuthMsgFailed(#[source] WriteMsgError),
    #[error("Failed to read ClientAuthMsg response")]
    ReadClientAuthMsgResponseFailed(#[source] ReadMsgError),
    #[error("Failed to deserialize ClientAuthMsg response")]
    DeserializeClientAuthMsgResponseFailed(#[source] ProtobufError),
    #[error("Did not receive expected ClientAuthMsg response")]
    ClientAuthMsgResponseUnexpected(proto::handshake::HandshakeMsg),
}
pub struct KeyOptions<
    ReportPinError,
    ReportPinFunction,
    ReportPinResult,
    PrivateKey,
    PrivateKeyError,
    PrivateKeyFunction,
    PrivateKeyResult,
    PublicKey,
> where
    ReportPinFunction: FnOnce(&str) -> ReportPinResult,
    ReportPinResult: Future<Output = Result<(), ReportPinError>>,
    PrivateKeyFunction: FnOnce() -> PrivateKeyResult,
    PrivateKeyResult: Future<Output = Result<PrivateKey, PrivateKeyError>>,
    PrivateKey: AsRef<[u8]>,
    PublicKey: AsRef<[u8]>,
{
    pub public_key: PublicKey,
    pub report_pin_fn: ReportPinFunction,
    pub private_key_fn: PrivateKeyFunction,
}

// TODO: recoverable errors should return the connection
pub async fn handshake<
    Connection,
    ReportPinError,
    ReportPinResult,
    ReportPinFunction,
    Password,
    PasswordError,
    PasswordFunction,
    PasswordResult,
    PrivateKey,
    PrivateKeyError,
    PrivateKeyResult,
    PrivateKeyFunction,
    PublicKey,
>(
    mut conn: Connection,
    name: Chars,
    key: Option<
        KeyOptions<
            ReportPinError,
            ReportPinFunction,
            ReportPinResult,
            PrivateKey,
            PrivateKeyError,
            PrivateKeyFunction,
            PrivateKeyResult,
            PublicKey,
        >,
    >,
    password_fn: Option<PasswordFunction>,
) -> Result<
    (
        proto::handshake::ConnectionSuccessfulMsg,
        Session<Connection>,
    ),
    HandshakeError<ReportPinError, PrivateKeyError, PasswordError>,
>
where
    // TODO: This is not a nice API to mock, and isn't the best API for uring.
    // It would be nice to be able to use a channel for local and testing.
    // They have a difference in constraints that make awkward APIs though.
    // let buf = con.send(buf).await?.unwrap_or_default();
    // let buf = con.recv(if con.uses_buf() { Some(buf) } else { None }).await?;
    Connection: futures_io::AsyncRead + futures_io::AsyncWrite + Unpin,
    ReportPinFunction: FnOnce(&str) -> ReportPinResult,
    ReportPinResult: Future<Output = Result<(), ReportPinError>>,
    PrivateKeyFunction: FnOnce() -> PrivateKeyResult,
    PrivateKeyResult: Future<Output = Result<PrivateKey, PrivateKeyError>>,
    PrivateKey: AsRef<[u8]> + Sized,
    ReportPinError: std::error::Error + 'static,
    Password: AsRef<[u8]>,
    PasswordError: std::error::Error + 'static,
    PasswordFunction: FnOnce() -> PasswordResult,
    PasswordResult: Future<Output = Result<Password, PasswordError>>,
    PublicKey: AsRef<[u8]> + Sized,
    PrivateKeyError: std::error::Error + 'static,
{
    use HandshakeError::*;
    let mut msgbuf = Vec::new();

    let (public_key, report_pin_fn, private_key_fn) = match key {
        None => (None, None, None),
        Some(KeyOptions {
            public_key,
            report_pin_fn,
            private_key_fn,
        }) => (Some(public_key), Some(report_pin_fn), Some(private_key_fn)),
    };
    let public_key_md5 = public_key
        .as_ref()
        .map_or_else(Chars::new, |bytes| md5sum_pubkey(bytes).into());

    let mut req_handshake_msg = proto::handshake::HandshakeMsg::default();
    let client_init_msg = req_handshake_msg.mut_client_init_msg();
    client_init_msg.set_player_name(name.clone());
    client_init_msg.set_version(
        Chars::from_bytes(Bytes::from_static(crate::VERSION.as_bytes()))
            .expect("Chars initialized from str should be valid UTF8"),
    );
    client_init_msg.set_public_key_md5(public_key_md5);

    log::debug!("Sending {:?}", &req_handshake_msg);
    msgbuf.clear();
    req_handshake_msg
        .write_to_vec(&mut msgbuf)
        .map_err(SerializeClientInitMsgFailed)?;
    write_msg(&mut conn, &msgbuf)
        .await
        .map_err(WriteClientInitMsgFailed)?;

    log::debug!("Awaiting response");
    read_msg(&mut conn, &mut msgbuf)
        .await
        .map_err(ReadClientInitMsgResponseFailed)?;
    let mut rsp_handshake_msg = proto::handshake::HandshakeMsg::parse_from_bytes(&msgbuf)
        .map_err(DeserializeClientInitMsgResponseFailed)?;
    log::debug!("Response: {:?}", &rsp_handshake_msg);

    if rsp_handshake_msg.has_request_public_key_msg() {
        let (public_key, report_pin_fn) =
            public_key.zip(report_pin_fn).ok_or(PublicKeyRequested)?;
        let client_init_msg = req_handshake_msg.take_client_init_msg();
        let request_public_key_msg = rsp_handshake_msg.take_request_public_key_msg();
        report_pin_fn(request_public_key_msg.get_pin())
            .await
            .map_err(ReportPinFailed)?;

        let mut maptool_pubkey_bytes = BytesMut::new();
        write!(
            &mut maptool_pubkey_bytes,
            "{}",
            crate::format_pubkey(&public_key)
        )
        .expect("Serialized pubkey should be shorter than maximum buffer size");
        let maptool_pubkey_text = Chars::from_bytes(maptool_pubkey_bytes.freeze())
            .expect("format_pubkey should serialize utf8");
        let public_key_upload_msg = req_handshake_msg.mut_public_key_upload_msg();
        public_key_upload_msg.set_public_key(maptool_pubkey_text);

        log::debug!("Sending {:?}", &req_handshake_msg);
        msgbuf.clear();
        req_handshake_msg
            .write_to_vec(&mut msgbuf)
            .map_err(SerializePublicKeyUploadMsgFailed)?;
        write_msg(&mut conn, &msgbuf)
            .await
            .map_err(WritePublicKeyUploadMsgFailed)?;

        let pubkey_text = req_handshake_msg
            .mut_public_key_upload_msg()
            .take_public_key();
        log::debug!("Awaiting response");
        read_msg(&mut conn, &mut msgbuf)
            .await
            .map_err(ReadPublicKeyUploadMsgResponseFailed)?;
        rsp_handshake_msg = proto::handshake::HandshakeMsg::parse_from_bytes(&msgbuf)
            .map_err(DeserializePublicKeyUploadMsgResponseFailed)?;
        log::debug!("Response: {:?}", &rsp_handshake_msg);

        if !rsp_handshake_msg.has_public_key_added_msg() {
            return Err(PublicKeyUploadMsgResponseUnexpected(rsp_handshake_msg));
        }

        let response_pubkey_text = rsp_handshake_msg
            .mut_public_key_added_msg()
            .take_public_key();
        if response_pubkey_text != pubkey_text {
            return Err(PublicKeyUploadMsgResponseKeyMismatch {
                sent: pubkey_text,
                received: response_pubkey_text,
            });
        }

        req_handshake_msg.set_client_init_msg(client_init_msg);

        log::debug!("Sending {:?}", &req_handshake_msg);
        msgbuf.clear();
        req_handshake_msg
            .write_to_vec(&mut msgbuf)
            .map_err(SerializeClientInitMsgFailed)?;
        write_msg(&mut conn, &msgbuf)
            .await
            .map_err(WriteClientInitMsgFailed)?;

        log::debug!("Awaiting response");
        read_msg(&mut conn, &mut msgbuf)
            .await
            .map_err(ReadClientInitMsgResponseFailed)?;
        rsp_handshake_msg = proto::handshake::HandshakeMsg::parse_from_bytes(&msgbuf)
            .map_err(DeserializeClientInitMsgResponseFailed)?;
        log::debug!("Response: {:?}", &rsp_handshake_msg);
    }

    if !rsp_handshake_msg.has_use_auth_type_msg() {
        return Err(ClientInitMsgResponseUnexpected(rsp_handshake_msg));
    }

    let use_auth_type = rsp_handshake_msg.mut_use_auth_type_msg();
    let auth_type = use_auth_type.get_auth_type();
    let salt = use_auth_type.take_salt();
    let mut challenges = use_auth_type.take_challenge();
    let client_auth = req_handshake_msg.mut_client_auth_message();
    match auth_type {
        proto::handshake::AuthTypeEnum::SHARED_PASSWORD => {
            let iv = use_auth_type.take_iv();
            let password_fn = password_fn.ok_or(PasswordRequested)?;
            let password = password_fn().await.map_err(GetPasswordFailed)?;

            let params = crate::secrets::pbkdf2_with_hmac_sha1_aes128cbc(
                &salt,
                iv.as_ref().try_into().map_err(WrongIvLength)?,
            );

            let mut err = Ok(());
            for challenge in challenges {
                match params.decrypt(&password, &challenge) {
                    Err(e) => {
                        err = Err(PasswordDecryptFailed(format!("{}", e)));
                        continue;
                    }
                    Ok(mut plaintext) => {
                        log::debug!("Challenge: {}", &String::from_utf8_lossy(&plaintext));
                        if !plaintext.starts_with(name.as_bytes()) {
                            err = Err(DecryptedChallengeUnexpected(
                                String::from_utf8_lossy(&plaintext).to_string(),
                            ));
                            continue;
                        }
                        let nonce = &mut plaintext[name.as_bytes().len()..];
                        nonce.reverse();
                        let iv = rand::random();
                        let params = crate::secrets::pbkdf2_with_hmac_sha1_aes128cbc(&salt, &iv);
                        let challenge_response = match params.encrypt(&password, nonce) {
                            Err(e) => {
                                err = Err(PasswordEncryptFailed(format!("{}", e)));
                                continue;
                            }
                            Ok(challenge_response) => challenge_response,
                        };
                        client_auth.set_challenge_response(challenge_response.into());
                        client_auth.set_iv(Bytes::copy_from_slice(&iv));
                        err = Ok(());
                        break;
                    }
                }
            }
            err?;
        }
        proto::handshake::AuthTypeEnum::ASYMMETRIC_KEY => {
            let private_key_fn = private_key_fn.ok_or(PublicKeyRequested)?;
            let private_key = private_key_fn().await.map_err(GetPrivateKeyFailed)?;
            let private_key = RsaPrivateKey::from_pkcs1_der(private_key.as_ref())
                .map_err(DeserializePrivateKeyFailed)?;
            // The "RSA" algorithm is RSA in ECB mode with PKCS1 padding.
            // This happens to be what the rsa module implements
            let challenge = challenges.remove(0);
            let mut plaintext = private_key
                .decrypt(rsa::padding::PaddingScheme::PKCS1v15Encrypt, &challenge)
                .map_err(KeyDecryptFailed)?;
            log::debug!("Challenge: {}", &String::from_utf8_lossy(&plaintext));
            if !plaintext.starts_with(name.as_bytes()) {
                return Err(DecryptedChallengeUnexpected(
                    String::from_utf8_lossy(&plaintext).to_string(),
                ));
            }
            let nonce = &mut plaintext[name.as_bytes().len()..];
            nonce.reverse();
            log::debug!("Response: {}", &String::from_utf8_lossy(&nonce));
            client_auth.set_challenge_response(nonce.to_owned().into());
        }
    }

    log::debug!("Sending {:?}", &req_handshake_msg);
    msgbuf.clear();
    req_handshake_msg
        .write_to_vec(&mut msgbuf)
        .map_err(SerializeClientAuthMsgFailed)?;
    write_msg(&mut conn, &msgbuf)
        .await
        .map_err(WriteClientAuthMsgFailed)?;

    log::debug!("Awaiting response");
    read_msg(&mut conn, &mut msgbuf)
        .await
        .map_err(ReadClientAuthMsgResponseFailed)?;
    rsp_handshake_msg = proto::handshake::HandshakeMsg::parse_from_bytes(&msgbuf)
        .map_err(DeserializeClientAuthMsgResponseFailed)?;
    log::debug!("Response {:?}", &rsp_handshake_msg);

    if !rsp_handshake_msg.has_connection_successful_msg() {
        return Err(ClientAuthMsgResponseUnexpected(rsp_handshake_msg));
    }

    Ok((
        rsp_handshake_msg.take_connection_successful_msg(),
        Session { conn },
    ))
}
