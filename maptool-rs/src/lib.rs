use core::{
    fmt,
    pin::Pin,
    task::{Context, Poll},
};
use std::{
    io::{Error as IoError, ErrorKind as IoErrorKind},
    net::ToSocketAddrs,
};

use async_trait::async_trait;
use base64ct::{Base64, Encoding};
use thiserror::Error;
use tokio::net::TcpStream;

pub use maptool_types::{DirectServerAddress, ServerAddress, WebRtcServerAddress};

pub const SOURCE_OFFER: &'static [u8] =
    include_bytes!(env!(concat!(env!("CARGO_PKG_NAME"), "_SOURCE_OFFER")));

pub mod handshake;
mod proto {
    include!(concat!(env!("OUT_DIR"), "/proto/mod.rs"));
}
pub(crate) mod secrets;
pub mod webrtc;

pub(crate) const VERSION: &'static str = "1.11.4";

pub struct MapToolFormattedPublicKey<'a> {
    key: &'a [u8],
}

const KEY_PREFIX: &'static str = "====== Begin Public Key ======";
const KEY_SUFFIX: &'static str = "====== End Public Key ======";

impl<'a> fmt::Display for MapToolFormattedPublicKey<'a> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}\n", KEY_PREFIX)?;
        for chunk in self.key.chunks(60) {
            let mut enc_buf = [0u8; 80];
            let encoded =
                Base64::encode(chunk, &mut enc_buf).expect("60 byte chunks encode to 80 bytes");
            write!(f, "{}\n", encoded)?;
        }
        write!(f, "{}\n", KEY_SUFFIX)
    }
}

pub fn format_pubkey<'a, K>(key: &'a K) -> MapToolFormattedPublicKey<'a>
where
    K: AsRef<[u8]>,
{
    MapToolFormattedPublicKey { key: key.as_ref() }
}

#[pin_project::pin_project(project = ServerConnectionProj)]
pub enum ServerConnection {
    Tcp(#[pin] TcpStream),
    DataChannel(#[pin] webrtc::SingletonWebRtcDataChannelMessageStream),
}

#[async_trait]
pub trait Connect {
    type Error;
    async fn connect(&self) -> Result<ServerConnection, Self::Error>;
}

#[async_trait]
impl<DirectAddress> Connect for DirectServerAddress<DirectAddress>
where
    DirectAddress: ToSocketAddrs + Send + Sync,
{
    type Error = IoError;
    async fn connect(&self) -> Result<ServerConnection, Self::Error> {
        // TODO: This blocks to resolve names,
        // but tokio::net::ToSocketAddrs is executor specific
        // NOTE: ToSocketAddrs::Iter isn't send,
        // so it's collected into a Vec whose iter is.
        let addrs: Vec<_> = self.address.to_socket_addrs()?.collect();

        let mut r = Err(IoError::new(
            IoErrorKind::InvalidInput,
            "could not resolve to any addresses",
        ));
        for addr in addrs {
            r = TcpStream::connect(addr).await;
            if r.is_ok() {
                break;
            }
        }

        Ok(ServerConnection::Tcp(r?))
    }
}

#[async_trait]
impl<Name, ServerName> Connect for WebRtcServerAddress<Name, ServerName>
where
    Name: AsRef<str> + Send + Sync,
    ServerName: AsRef<str> + Send + Sync,
{
    type Error = webrtc::ConnectError;
    async fn connect(&self) -> Result<ServerConnection, Self::Error> {
        let conn = webrtc::Signaller::builder()
            .id(self.name.as_ref())
            .server_name(self.server_name.as_ref())
            .build()
            .connect()
            .await?;
        Ok(ServerConnection::DataChannel(conn))
    }
}

#[derive(Debug, Error)]
#[cfg_attr(feature = "miette", derive(miette::Diagnostic))]
pub enum ServerConnectError {
    #[error("Failed to connect to TCP socket")]
    TcpConnectFailed(#[source] IoError),
    #[error("Failed to connect to WebRTC DataChannel")]
    WebRtcConnectFailed(#[source] webrtc::ConnectError),
}

#[async_trait]
impl<DirectAddress, Name, ServerName> Connect for ServerAddress<DirectAddress, Name, ServerName>
where
    DirectAddress: ToSocketAddrs + Send + Sync,
    Name: AsRef<str> + Send + Sync,
    ServerName: AsRef<str> + Send + Sync,
{
    type Error = ServerConnectError;
    async fn connect(&self) -> Result<ServerConnection, Self::Error> {
        use ServerConnectError::*;
        match self {
            ServerAddress::Direct(address) => {
                Ok(address.connect().await.map_err(TcpConnectFailed)?)
            }
            ServerAddress::WebRtc(address) => {
                Ok(address.connect().await.map_err(WebRtcConnectFailed)?)
            }
        }
    }
}

impl ServerConnection {
    pub async fn connect<C>(address: C) -> Result<Self, C::Error>
    where
        C: Connect,
    {
        address.connect().await
    }
}

impl tokio::io::AsyncRead for ServerConnection {
    fn poll_read(
        self: Pin<&mut Self>,
        cx: &mut Context<'_>,
        buf: &mut tokio::io::ReadBuf<'_>,
    ) -> Poll<std::io::Result<()>> {
        match self.project() {
            ServerConnectionProj::Tcp(s) => s.poll_read(cx, buf),
            ServerConnectionProj::DataChannel(d) => d.poll_read(cx, buf),
        }
    }
}

impl tokio::io::AsyncWrite for ServerConnection {
    fn poll_write(
        self: Pin<&mut Self>,
        cx: &mut Context<'_>,
        buf: &[u8],
    ) -> Poll<std::io::Result<usize>> {
        match self.project() {
            ServerConnectionProj::Tcp(s) => s.poll_write(cx, buf),
            ServerConnectionProj::DataChannel(d) => d.poll_write(cx, buf),
        }
    }
    fn poll_flush(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<std::io::Result<()>> {
        match self.project() {
            ServerConnectionProj::Tcp(s) => s.poll_flush(cx),
            ServerConnectionProj::DataChannel(d) => d.poll_flush(cx),
        }
    }
    fn poll_shutdown(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<std::io::Result<()>> {
        match self.project() {
            ServerConnectionProj::Tcp(s) => s.poll_shutdown(cx),
            ServerConnectionProj::DataChannel(d) => d.poll_shutdown(cx),
        }
    }
}
