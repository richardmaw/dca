use std::{collections::HashMap, convert::{TryFrom, TryInto}};

use der::{asn1::Any as Asn1Any, Decodable, Document, Encodable, Error as DerError};
use miette::Diagnostic;
use pem_rfc7468::Error as PemError;
use pkcs5::{EncryptionScheme, Error as Pkcs5Error, pbes2::{EncryptionScheme as Pbes2EncryptionScheme, Parameters as Pbes2Parameters, Pbkdf2Params, Pbkdf2Prf}};
use pkcs7::{
    encrypted_data_content::{EncryptedDataContent, Version},
    enveloped_data_content::EncryptedContentInfo,
    ContentInfo, ContentType,
};
use sha2::digest::Digest;
use spki::AlgorithmIdentifier;
use secret_service::{EncryptionType, SecretService};
use thiserror::Error;

use crate::config::{Config, HashDigest, Hash, AppPasswordInfo, Salt};
use crate::pkcs7_content_info::ContentInfoDocument;

// TODO: Reduce coupling between this helper and config?

#[cfg(feature = "secret-service")]
#[derive(Debug, Diagnostic, Error)]
enum GetSecretServicePasswordError {
    #[error("Unable to get default secret collection")]
    GetCollectionError(#[source] secret_service::Error),
    #[error("Unable to search secret collection")]
    SearchError(#[source] secret_service::Error),
    #[error("Multiple passwords found")]
    AmbiguousResultsError,
    #[error("Unable to unlock item")]
    UnlockError(#[source] secret_service::Error),
    #[error("Unable to read secret")]
    ReadError(#[source] secret_service::Error),
}

#[cfg(feature = "secret-service")]
fn get_secret_service_password(
    ß: &mut SecretService,
) -> Result<Option<Vec<u8>>, GetSecretServicePasswordError> {
    let collection = ß
        .get_default_collection()
        .map_err(GetSecretServicePasswordError::GetCollectionError)?;
    let attributes = HashMap::from([
        ("application", "name.maw.richard.dca"),
        ("xdg:schema", "org.gnome.keyring.ChainedKeyring"),
    ]);
    let matches = collection
        .search_items(attributes)
        .map_err(GetSecretServicePasswordError::SearchError)?;
    match matches.as_slice() {
        [] => Ok(None),
        [item] => {
            item.ensure_unlocked()
                .map_err(GetSecretServicePasswordError::UnlockError)?;
            Ok(Some(
                item.get_secret()
                    .map_err(GetSecretServicePasswordError::ReadError)?,
            ))
        }
        [..] => Err(GetSecretServicePasswordError::AmbiguousResultsError),
    }
}

#[cfg(feature = "secret-service")]
#[derive(Debug, Diagnostic, Error)]
enum SetSecretServicePasswordError {
    #[error("Unable to get default secret collection")]
    GetCollectionError(#[source] secret_service::Error),
    #[error("Unable to set item")]
    SetItemError(#[source] secret_service::Error),
}

#[cfg(feature = "secret-service")]
fn set_secret_service_password(
    ß: &mut SecretService,
    secret: &[u8],
) -> Result<(), SetSecretServicePasswordError> {
    let collection = ß
        .get_default_collection()
        .map_err(SetSecretServicePasswordError::GetCollectionError)?;
    let label = "DCA";
    let attributes = HashMap::from([
        ("application", "name.maw.richard.dca"),
        ("xdg:schema", "org.gnome.keyring.ChainedKeyring"),
    ]);
    let replace = true;
    let content_type = "text/plain";
    collection
        .create_item(label, attributes, secret, replace, content_type)
        .map_err(SetSecretServicePasswordError::SetItemError)?;
    Ok(())
}

#[derive(Debug, Diagnostic, Error)]
pub(crate) enum GetPasswordError {
    #[cfg(feature = "secret-service")]
    #[error("Unable to get default secret collection")]
    GetCollectionError(#[source] secret_service::Error),
    #[cfg(feature = "secret-service")]
    #[error("Unable to search secret collection")]
    SearchError(#[source] secret_service::Error),
    #[cfg(feature = "secret-service")]
    #[error("Multiple passwords found")]
    AmbiguousResultsError,
    #[cfg(feature = "secret-service")]
    #[error("Unable to unlock item")]
    UnlockError(#[source] secret_service::Error),
    #[cfg(feature = "secret-service")]
    #[error("Unable to read secret")]
    ReadError(#[source] secret_service::Error),
    #[error("Unable to read input password")]
    InputError(#[source] std::io::Error),
    #[error("Input passwords do not match")]
    ConfirmError,
    #[error("Input password is incorrect")]
    IncorrectPasswordError,
    #[cfg(feature = "secret-service")]
    #[error("Unable to set item")]
    SetItemError(#[source] secret_service::Error),
}

#[cfg(feature = "secret-service")]
impl From<GetSecretServicePasswordError> for GetPasswordError {
    fn from(e: GetSecretServicePasswordError) -> Self {
        match e {
            GetSecretServicePasswordError::GetCollectionError(e) => Self::GetCollectionError(e),
            GetSecretServicePasswordError::SearchError(e) => Self::SearchError(e),
            GetSecretServicePasswordError::AmbiguousResultsError => Self::AmbiguousResultsError,
            GetSecretServicePasswordError::UnlockError(e) => Self::UnlockError(e),
            GetSecretServicePasswordError::ReadError(e) => Self::ReadError(e),
        }
    }
}

#[cfg(feature = "secret-service")]
impl From<SetSecretServicePasswordError> for GetPasswordError {
    fn from(e: SetSecretServicePasswordError) -> Self {
        match e {
            SetSecretServicePasswordError::GetCollectionError(e) => Self::GetCollectionError(e),
            SetSecretServicePasswordError::SetItemError(e) => Self::SetItemError(e),
        }
    }
}

fn hash_password(salt: Salt, password: &[u8]) -> HashDigest {
    let mut hasher = Hash::new();
    hasher.update(salt.to_le_bytes());
    hasher.update(password);
    hasher.finalize()
}

// TODO: Proper docstring
// Get the application password from secret service or input, saving if new.
// First tries the secret service.
// If not forthcoming from the secret service it is read from input.
// If we have a salted hash stored in config the password is checked against that
// and returned if matching or erroring if no match.
// If we have no stored salted hash then password is asked to be confirmed.
// If the secret service was working but none found the password is saved there.
// If no secret service is available a salted hash is stored in config.
pub(crate) fn get_password(
    config: &mut Config,
    #[cfg(feature = "secret-service")] skip_secret_service: bool,
) -> Result<Vec<u8>, GetPasswordError> {
    #[cfg(feature = "secret-service")]
    let ß = if !skip_secret_service {
        match SecretService::new(EncryptionType::Dh) {
            Ok(mut ß) => match get_secret_service_password(&mut ß)? {
                Some(password) => return Ok(password),
                None => Some(ß),
            },
            Err(_) => {
                log::warn!("Couldn't open secret service, falling back to reading");
                None
            }
        }
    } else {
        None
    };

    let pass = rpassword::read_password_from_tty(Some("Enter application password: "))
        .map_err(GetPasswordError::InputError)?;
    match config.password {
        Some(ref password) => {
            if hash_password(password.salt, pass.as_bytes()) == password.hash {
                return Ok(pass.into());
            } else {
                return Err(GetPasswordError::IncorrectPasswordError);
            }
        }
        None => {
            let confirm = rpassword::read_password_from_tty(Some("Confirm password: "))
                .map_err(GetPasswordError::InputError)?;
            if pass != confirm {
                return Err(GetPasswordError::ConfirmError);
            }
        }
    }

    #[cfg(feature = "secret-service")]
    if let Some(mut ß) = ß {
        set_secret_service_password(&mut ß, pass.as_bytes())?;
        return Ok(pass.into());
    }

    let salt = rand::random();
    let hash = hash_password(salt, pass.as_bytes());
    config.password = Some(AppPasswordInfo { hash, salt });

    Ok(pass.into())
}

#[derive(Debug, Diagnostic, Error)]
pub(crate) enum GetServerPasswordError {
    #[error("Unable to read input password")]
    InputError(#[source] std::io::Error),
    #[error("Input passwords do not match")]
    ConfirmError,
}

// TODO: Handle returned password securely in e.g. zeroized, locked, memfd_secret
pub(crate) fn get_server_password(server: &str) -> Result<Vec<u8>, GetServerPasswordError> {
    let pass = rpassword::read_password_from_tty(Some(&format!("Enter server password for {}: ", server)))
        .map_err(GetServerPasswordError::InputError)?;
    let confirm = rpassword::read_password_from_tty(Some("Confirm password: "))
        .map_err(GetServerPasswordError::InputError)?;
    if pass != confirm {
        return Err(GetServerPasswordError::ConfirmError);
    }

    Ok(pass.into())
}

#[derive(Debug, Diagnostic, Error)]
pub(crate) enum EncryptServerPasswordError {
    #[error("Failed to initialize PBES2 scrypt aes256cbc parameters: {0}")]
    ParametersInitializeFailure(Pkcs5Error),
    #[error("Failed to encrypt server password: {0}")]
    EncryptionFailure(Pkcs5Error),
    #[error("Failed to encode content as DER")]
    DerEncodeFailure(#[from] DerError),
    #[error("Failed to encode DER as PEM")]
    PemEncodeFailure(#[from] PemError),
}

pub(crate) fn encrypt_server_password(password: &[u8], app_password: &[u8]) -> Result<ContentInfoDocument, EncryptServerPasswordError> {
    type E = EncryptServerPasswordError;
    let salt: [u8; 16] = rand::random();
    let iv: [u8; 16] = rand::random();
    let encryption_scheme: EncryptionScheme =
        Pbes2Parameters::scrypt_aes256cbc(Default::default(), &salt, &iv)
            .map_err(E::ParametersInitializeFailure)?
            .into();

    let encrypted = encryption_scheme
        .encrypt(app_password, password)
        .map_err(E::EncryptionFailure)?;

    // TODO: Watch out for an implementation of EncryptionScheme → AlgorithmIdentifier
    let mut alg_buf = Vec::new();
    let oid = encryption_scheme.oid();
    let parameters = encryption_scheme.pbes2().expect("Initialised PBES2");
    let encoded_len: usize = parameters
        .encoded_len()
        .expect("Programmatically constructed parameters should have an encoded length")
        .try_into()
        .expect("Encoded length should not overflow");
    alg_buf.resize(encoded_len, 0u8);
    let mut encoder = der::Encoder::new(alg_buf.as_mut_slice());
    parameters
        .encode(&mut encoder)
        .expect("Programmatically defined encryption should be encodable");
    let encoded_der = encoder
        .finish()
        .expect("Programmatically defined encryption should be encodable");
    let any =
        Asn1Any::from_der(encoded_der).expect("We should have just encoded this ourselves");
    let parameters = Some(any);

    let content_encryption_algorithm = AlgorithmIdentifier { oid, parameters };

    let encrypted_content_info = EncryptedContentInfo {
        content_type: ContentType::EncryptedData,
        content_encryption_algorithm,
        encrypted_content: Some(&encrypted),
    };
    let encrypted_data_content = EncryptedDataContent {
        version: Version::V0,
        encrypted_content_info,
    };
    let content_info = ContentInfo::EncryptedData(Some(encrypted_data_content));
    Ok(content_info.try_into()?)
}

#[derive(Debug, Diagnostic, Error)]
pub(crate) enum DecryptServerPasswordError {
    #[error("PKCS7 ContentInfo does not contain encrypted data")]
    ContentInfoNotEncryptedData,
    #[error("PKCS7 ContentInfo EncryptedData has no content")]
    EncryptedDataMissingContent,
    #[error("EncryptedData not V0")]
    EncryptedDataNotV0,
    #[error("Encryption scheme unparseable")]
    EncryptionSchemeUnparseable(#[source] DerError),
    #[error("PKCS7 ContentInfo EncryptedData has no content")]
    EncryptedContentMissingContent,
    #[error("Password decryption failed: {0}")]
    PasswordDecryptFailure(Pkcs5Error),
}

// TODO: Handle returned password securely in e.g. zeroized, locked, memfd_secret
pub(crate) fn decrypt_server_password(content_info: &ContentInfoDocument, app_password: &[u8]) -> Result<Vec<u8>, DecryptServerPasswordError> {
    type E = DecryptServerPasswordError;
    let encrypted_data_content = match content_info.decode() {
        ContentInfo::EncryptedData(encrypted_data_content) => encrypted_data_content,
        _ => return Err(E::ContentInfoNotEncryptedData),
    };
    let encrypted_data_content =
        encrypted_data_content.ok_or(E::EncryptedDataMissingContent)?;

    let encrypted_content_info = match encrypted_data_content {
        EncryptedDataContent {
            version: Version::V0,
            encrypted_content_info,
        } => encrypted_content_info,
        #[allow(unreachable_patterns)]
        _ => return Err(E::EncryptedDataNotV0),
    };

    let scheme =
        EncryptionScheme::try_from(encrypted_content_info.content_encryption_algorithm)
            .map_err(E::EncryptionSchemeUnparseable)?;
    let encrypted_content = encrypted_content_info.encrypted_content.ok_or(E::EncryptedContentMissingContent)?;
    scheme.decrypt(app_password, encrypted_content).map_err(E::PasswordDecryptFailure)
}

pub(crate) fn pbkdf2_with_hmac_sha1_aes128cbc<'a>(salt: &'a [u8], iv: &'a [u8; 16]) -> Pbes2Parameters<'a> {
    const KEY_ITERATION_KEY_COUNT: u32 = 2000;
    let kdf = Pbkdf2Params {
        salt: salt,
        iteration_count: KEY_ITERATION_KEY_COUNT,
        key_length: None, // TODO should this be 128 bit i.e. 16?
        prf: Pbkdf2Prf::HmacWithSha1,
    }
    .into();
    let encryption = Pbes2EncryptionScheme::Aes128Cbc { iv };
    Pbes2Parameters { kdf, encryption }
}
