// TODO: Something like this should exist in pkcs7
use core::{convert::{TryFrom, TryInto}, fmt, str::FromStr};

use der::{Decodable, Document, Encodable};
use pem_rfc7468::PemLabel;
use pkcs7::ContentInfo;
use serde::{Deserialize, Deserializer, Serialize, Serializer};
use zeroize::{Zeroize, Zeroizing};

#[derive(Clone, Debug)]
pub(crate) struct ContentInfoDocument(Zeroizing<Vec<u8>>);

impl PemLabel for ContentInfoDocument {
    const TYPE_LABEL: &'static str = "PKCS7";
}

impl<'a> Document<'a> for ContentInfoDocument {
    type Message = ContentInfo<'a>;
    // The data should be encrypted, but it may be crackable,
    // so you should prefer to keep it secret I guess.
    const SENSITIVE: bool = true;
}

impl AsRef<[u8]> for ContentInfoDocument {
    fn as_ref(&self) -> &[u8] {
        self.0.as_ref()
    }
}

impl TryFrom<ContentInfo<'_>> for ContentInfoDocument {
    type Error = der::Error;

    fn try_from(key: ContentInfo<'_>) -> der::Result<ContentInfoDocument> {
        ContentInfoDocument::try_from(&key)
    }
}

impl TryFrom<&ContentInfo<'_>> for ContentInfoDocument {
    type Error = der::Error;

    fn try_from(content_info: &ContentInfo<'_>) -> der::Result<ContentInfoDocument> {
        Ok(content_info.to_vec()?.try_into()?)
    }
}

impl TryFrom<Vec<u8>> for ContentInfoDocument {
    type Error = der::Error;

    fn try_from(mut bytes: Vec<u8>) -> der::Result<Self> {
        if let Err(err) = <Self as Document>::Message::from_der(bytes.as_slice()) {
            bytes.zeroize();
            return Err(err);
        }

        Ok(Self(Zeroizing::new(bytes)))
    }
}

impl TryFrom<&[u8]> for ContentInfoDocument {
    type Error = der::Error;

    fn try_from(bytes: &[u8]) -> der::Result<Self> {
        Ok(Self::from_der(bytes)?)
    }
}

impl fmt::Display for ContentInfoDocument {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        // TODO: If pem library gets an encode to writer method can avoid alloc
        write!(f, "{}", &self.to_pem(Default::default()).map_err(|_| fmt::Error)?)
    }
}

impl FromStr for ContentInfoDocument {
    type Err = der::Error;

    fn from_str(s: &str) -> der::Result<Self> {
        Ok(Self::from_pem(s)?)
    }
}

impl<'de> Deserialize<'de> for ContentInfoDocument {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        serde_with::rust::display_fromstr::deserialize(deserializer)
    }
}

impl Serialize for ContentInfoDocument {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        serde_with::rust::display_fromstr::serialize(self, serializer)
    }
}
