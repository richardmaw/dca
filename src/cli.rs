use std::{borrow::Cow, path::PathBuf};

use clap::{Args, Parser};

#[derive(Args, Debug)]
pub(crate) struct ConfigFile {
    #[clap(short = 'c', long = "config", env=concat!(env!("ENV_PREFIX"), "DCA_CONFIG"))]
    path: Option<PathBuf>,
}

impl ConfigFile {
    pub(crate) fn path(&self) -> Cow<'_, PathBuf> {
        match self.path {
            Some(ref path) => Cow::Borrowed(path),
            None => {
                let mut path = dirs::config_dir().unwrap_or(".".into());
                path.push("dca.toml");
                Cow::Owned(path)
            }
        }
    }
}

#[derive(Args, Debug)]
pub(crate) struct RequiredProfile {
    #[clap(short='P', long="profile", env=concat!(env!("ENV_PREFIX"), "DCA_PROFILE"))]
    pub(crate) id: String,
}

#[derive(Args, Debug)]
pub(crate) struct OptionalProfile {
    #[clap(short='P', long="profile", env=concat!(env!("ENV_PREFIX"), "DCA_PROFILE"), default_value="default")]
    pub(crate) id: String,
}

#[derive(Args, Debug)]
pub(crate) struct User {
    #[clap(value_name = "USER")]
    pub(crate) name: String,
}

#[derive(Args, Debug)]
struct Connect {
    #[clap(short, long)]
    address: String,
}

#[derive(Args, Debug)]
pub(crate) struct ListProfiles {
    #[clap(flatten)]
    pub(crate) config: ConfigFile,
}

#[derive(Args, Debug)]
pub(crate) struct DeleteProfile {
    #[clap(flatten)]
    pub(crate) config: ConfigFile,
    #[clap(flatten)]
    pub(crate) profile: RequiredProfile,
    #[clap(short, long)]
    pub(crate) force: bool,
    #[cfg(feature = "secret-service")]
    #[clap(short, long)]
    pub(crate) skip_secret_service: bool,
}

#[derive(Args, Debug)]
pub(crate) struct NameClient {
    #[clap(flatten)]
    pub(crate) config: ConfigFile,
    #[clap(flatten)]
    pub(crate) profile: OptionalProfile,
    #[clap(flatten)]
    pub(crate) user: User,
}

#[derive(Args, Debug)]
pub(crate) struct GenerateKey {
    #[clap(flatten)]
    pub(crate) config: ConfigFile,
    #[clap(flatten)]
    pub(crate) profile: OptionalProfile,
    #[cfg(feature = "secret-service")]
    #[clap(short, long)]
    pub(crate) skip_secret_service: bool,
}

#[derive(Args, Debug)]
pub(crate) struct GetKey {
    #[clap(flatten)]
    pub(crate) config: ConfigFile,
    #[clap(flatten)]
    pub(crate) profile: OptionalProfile,
}

#[derive(Args, Debug)]
pub(crate) struct DeleteKey {
    #[clap(flatten)]
    pub(crate) config: ConfigFile,
    #[clap(flatten)]
    pub(crate) profile: OptionalProfile,
    #[clap(short, long)]
    pub(crate) force: bool,
}

#[derive(Args, Debug)]
pub(crate) struct TestKeyEncrypt {
    #[clap(flatten)]
    pub(crate) config: ConfigFile,
    #[clap(flatten)]
    pub(crate) profile: OptionalProfile,
    #[clap(short, long)]
    pub(crate) input: Option<PathBuf>,
    #[clap(short, long)]
    pub(crate) output: Option<PathBuf>,
}

#[derive(Args, Debug)]
pub(crate) struct TestKeyDecrypt {
    #[clap(flatten)]
    pub(crate) config: ConfigFile,
    #[clap(flatten)]
    pub(crate) profile: OptionalProfile,
    #[clap(short, long)]
    pub(crate) input: Option<PathBuf>,
    #[clap(short, long)]
    pub(crate) output: Option<PathBuf>,
    #[cfg(feature = "secret-service")]
    #[clap(short, long)]
    pub(crate) skip_secret_service: bool,
}

#[derive(Args, Debug)]
pub(crate) struct TestKeySign {
    #[clap(flatten)]
    pub(crate) config: ConfigFile,
    #[clap(flatten)]
    pub(crate) profile: OptionalProfile,
    #[clap(short, long)]
    pub(crate) input: Option<PathBuf>,
    #[cfg(feature = "secret-service")]
    #[clap(short, long)]
    pub(crate) skip_secret_service: bool,
}

#[derive(Args, Debug)]
pub(crate) struct TestKeyVerify {
    #[clap(flatten)]
    pub(crate) config: ConfigFile,
    #[clap(flatten)]
    pub(crate) profile: OptionalProfile,
    #[clap(parse(try_from_str = hex::decode))]
    pub(crate) hash: std::vec::Vec<u8>,
    #[clap(parse(try_from_str = hex::decode))]
    pub(crate) signature: std::vec::Vec<u8>,
}

#[derive(Debug, Parser)]
pub(crate) enum KeyTest {
    Encrypt(TestKeyEncrypt),
    Decrypt(TestKeyDecrypt),
    Sign(TestKeySign),
    Verify(TestKeyVerify),
}

#[derive(Debug, Parser)]
pub(crate) enum Key {
    Generate(GenerateKey),
    Get(GetKey),
    Delete(DeleteKey),
    #[clap(subcommand)]
    Test(KeyTest),
}

#[derive(Args, Debug)]
pub(crate) struct SetPassword {
    #[clap(flatten)]
    pub(crate) config: ConfigFile,
    #[clap(flatten)]
    pub(crate) profile: OptionalProfile,
    #[cfg(feature = "secret-service")]
    #[clap(short, long)]
    pub(crate) skip_secret_service: bool,
    #[clap(short, long)]
    pub(crate) no_clobber: bool,
    pub(crate) server: String,
}

#[derive(Args, Debug)]
pub(crate) struct DeletePassword {
    #[clap(flatten)]
    pub(crate) config: ConfigFile,
    #[clap(flatten)]
    pub(crate) profile: OptionalProfile,
    pub(crate) server: String,
    #[clap(short, long)]
    pub(crate) force: bool,
}

#[derive(Args, Debug)]
pub(crate) struct TestPassword {
    #[clap(flatten)]
    pub(crate) config: ConfigFile,
    #[clap(flatten)]
    pub(crate) profile: OptionalProfile,
    #[cfg(feature = "secret-service")]
    #[clap(short, long)]
    pub(crate) skip_secret_service: bool,
    pub(crate) server: String,
    #[clap(parse(try_from_str = hex::decode))]
    pub(crate) salt: std::vec::Vec<u8>,
    #[clap(parse(try_from_str = hex::FromHex::from_hex))]
    pub(crate) iv: [u8; 16],
    #[clap(short, long)]
    pub(crate) input: Option<PathBuf>,
    #[clap(short, long)]
    pub(crate) output: Option<PathBuf>,
    #[clap(short, long)]
    pub(crate) decrypt: bool,
}

#[derive(Debug, Parser)]
pub(crate) enum Password {
    Set(SetPassword),
    Delete(DeletePassword),
    Test(TestPassword),
}

#[derive(Debug, Parser)]
pub(crate) enum Profile {
    List(ListProfiles),
    Delete(DeleteProfile),
}

#[derive(Debug, Parser)]
pub(crate) enum Client {
    Name(NameClient),
    #[clap(subcommand)]
    Key(Key),
}

#[derive(Args, Debug)]
pub(crate) struct SetServerKind {
    #[clap(flatten)]
    pub(crate) config: ConfigFile,
    #[clap(flatten)]
    pub(crate) profile: OptionalProfile,
    pub(crate) server: String,
    #[clap(arg_enum)]
    pub(crate) kind: crate::config::ServerKind,
}

#[derive(Debug, Parser)]
pub(crate) enum ServerKind {
    Set(SetServerKind),
}

#[derive(Debug, Parser)]
pub(crate) enum Server {
    #[clap(subcommand)]
    Kind(ServerKind),
    #[clap(subcommand)]
    Password(Password),
}

#[derive(Args, Debug)]
pub(crate) struct HelloWorld {
    #[clap(flatten)]
    pub(crate) config: ConfigFile,
    #[clap(flatten)]
    pub(crate) profile: OptionalProfile,
    #[cfg(feature = "secret-service")]
    #[clap(short, long)]
    pub(crate) skip_secret_service: bool,
    pub(crate) server: String,
}

#[derive(Debug, Parser)]
pub(crate) enum DCA {
    Install {
        /// Path to install links to
        #[clap(default_value = env!("BINDIR"))]
        path: PathBuf,
        #[cfg(any(unix, windows))]
        #[clap(short, long)]
        symbolic: bool,
    },
    GenerateCompletions {
        #[clap(arg_enum)]
        shell: clap_complete::Shell,
        #[clap(short, long)]
        output: Option<PathBuf>,
        #[clap(short, long)]
        no_clobber: bool,
    },
    #[clap(subcommand)]
    Profile(Profile),
    #[clap(subcommand)]
    Client(Client),
    #[clap(subcommand)]
    Server(Server),
    HelloWorld(HelloWorld),
    Connection,
    State,
    Sheet,
}

#[derive(Debug, Parser)]
#[clap(setting = clap::AppSettings::Multicall)]
pub(crate) enum Applet {
    #[clap(name = concat!(env!("PROGRAM_PREFIX"), "dca"), subcommand)]
    DCA(DCA),
    #[clap(name = concat!(env!("PROGRAM_PREFIX"), "dca-profile-list"))]
    ListProfiles(ListProfiles),
    #[clap(name = concat!(env!("PROGRAM_PREFIX"), "dca-profile-delete"))]
    DeleteProfile(DeleteProfile),
    #[clap(name = concat!(env!("PROGRAM_PREFIX"), "dca-client-name"))]
    NameClient(NameClient),
    #[clap(name = concat!(env!("PROGRAM_PREFIX"), "dca-client-key-generate"))]
    GenerateKey(GenerateKey),
    #[clap(name = concat!(env!("PROGRAM_PREFIX"), "dca-client-key-get"))]
    GetKey(GetKey),
    #[clap(name = concat!(env!("PROGRAM_PREFIX"), "dca-client-key-delete"))]
    DeleteKey(DeleteKey),
    #[clap(name = concat!(env!("PROGRAM_PREFIX"), "dca-client-password-set"))]
    SetPassword(SetPassword),
    #[clap(name = concat!(env!("PROGRAM_PREFIX"), "dca-client-password-delete"))]
    DeletePassword(DeletePassword),
    #[clap(name = concat!(env!("PROGRAM_PREFIX"), "dca-client-password-test"))]
    TestPassword(TestPassword),
    #[clap(name = concat!(env!("PROGRAM_PREFIX"), "decay"))]
    Decay,
    #[clap(external_subcommand)]
    UnrecognisedApplet(Vec<String>),
}
