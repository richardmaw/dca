use core::future::Future;
use std::{
    collections::btree_map::Entry,
    env::current_exe,
    fs,
    io::{self, Read, Write},
    path::PathBuf,
};

use clap::{IntoApp, Parser};
use miette::{bail, miette, IntoDiagnostic, Result};
use pkcs8::DecodePrivateKey;
use rsa::{
    pkcs8::{FromPrivateKey, FromPublicKey, ToPrivateKey, ToPublicKey},
    PublicKey, RsaPrivateKey, RsaPublicKey,
};
use spki::DecodePublicKey;
use tokio_util::compat::TokioAsyncReadCompatExt;

use maptool::{DirectServerAddress, WebRtcServerAddress};
use maptool_types::ServerAddress;

mod cli;
mod config;
mod pkcs7_content_info;
mod secrets;

use cli::*;
use config::Config;

// // TODO: Environment variable for CLIENT_ID and CONN?
// dca client list # shows all client-ids
// dca client delete ID # clean client
// dca client name [--client-id ID] USER
// dca client key generate [--client-id ID]
// dca client key get [--client-id ID] # prints public key to terminal
// dca client password set [--client-id ID] SERVER # receives from terminal password prompt
// dca client password delete [--client-id ID] SERVER # remove password and only use key
// dca connection create [--connection CONN] SERVER
// dca connection log [--connection CONN] # Log the event stream to console
// # Using ID (or default first) and SERVER start a connection identified by CONN (or default)
// # connect to the server and perform the handshake
// # and construct a bounded event queue that only dequeues events if there's at least one consumer
// # and all present consumers must have acknowledged the message to dequeue it.
// dca connection start [--client-id ID] [--connection CONN] SERVER
// dca connection poll [--connection CONN] # receive an event from the queue in a way convenient to run in a loop
// dca connection say [--connection CONN] # Send a chat message
// dca connection disconnect [--connection CONN] # End the connection
// # Initialize a higher-level interface on top of the connection that tracks state
// dca state manager start [--connection CONN]
// dca state token macro list [--connection CONN] TOKEN_ID
// dca state token macro set [--connection CONN] TOKEN_ID MACRO_ID --name MACRO_NAME
// dca state token property update [--connection CONN] TOKEN_ID (PROPERTY VALUE)...
// dca sheet agent start [--agent AGENT]
// dca sheet agent watch [--agent AGENT] PATH

fn read_input(path: Option<PathBuf>) -> Result<Vec<u8>> {
    match path {
        Some(path) => fs::read(path).into_diagnostic(),
        None => {
            let mut buf = Vec::new();
            io::stdin().read_to_end(&mut buf).into_diagnostic()?;
            Ok(buf)
        }
    }
}

fn write_output(path: Option<PathBuf>, buf: &[u8]) -> Result<()> {
    match path {
        Some(path) => fs::write(path, &buf).into_diagnostic(),
        None => {
            io::stdout().write(&buf).into_diagnostic()?;
            Ok(())
        }
    }
}

fn main() -> Result<()> {
    env_logger::init();
    let applet = Applet::parse();
    match applet {
        Applet::DCA(DCA::Install {
            path,
            #[cfg(any(unix, windows))]
            symbolic,
        }) => {
            #[cfg(unix)]
            use std::os::unix::fs::symlink;
            #[cfg(windows)]
            use std::os::windows::fs::symlink_file as symlink;

            let exe = current_exe().into_diagnostic()?;
            let mut target = path;

            for applet in Applet::into_app().get_subcommands() {
                let name = applet.get_name();
                if name == "install" {
                    continue;
                }
                target.push(name);
                #[cfg(any(unix, windows))]
                if symbolic {
                    symlink(&exe, &target).into_diagnostic()?;
                    target.pop();
                    continue;
                }
                fs::hard_link(&exe, &target).into_diagnostic()?;
                target.pop();
            }
        }
        Applet::DCA(DCA::GenerateCompletions {
            output,
            no_clobber,
            shell,
        }) => {
            let mut _stdout = None;
            let mut _file = None;
            let output: &mut dyn Write = match output {
                None => {
                    _stdout = Some(io::stdout());
                    _stdout.as_mut().unwrap()
                }
                Some(path) => {
                    let mut opts = std::fs::OpenOptions::new();
                    opts.write(true);
                    if no_clobber {
                        opts.create_new(true);
                    } else {
                        opts.create(true).truncate(true);
                    }
                    _file = Some(opts.open(path).into_diagnostic()?);
                    _file.as_mut().unwrap()
                }
            };
            for applet in Applet::into_app().get_subcommands_mut() {
                let name = applet.get_name().to_owned();
                clap_complete::generate(shell, applet, name, output);
            }
        }
        Applet::DCA(DCA::Profile(Profile::List(args))) | Applet::ListProfiles(args) => {
            let path = args.config.path();
            let config = Config::load(path.as_ref())?;
            for client in config.profiles.keys() {
                println!("{}", client);
            }
        }
        Applet::DCA(DCA::Profile(Profile::Delete(args))) | Applet::DeleteProfile(args) => {
            let path = args.config.path();
            let mut config = Config::load(path.as_ref())?;
            if let None = config.profiles.remove(&args.profile.id) {
                if !args.force {
                    bail!("Client {} not previously defined", &args.profile.id);
                }
            }
            // TODO: Delete any passwords or keys from secret store
            config.save(path.as_ref())?;
        }
        Applet::DCA(DCA::Client(Client::Name(args))) | Applet::NameClient(args) => {
            let path = args.config.path();
            let mut config = Config::load(path.as_ref())?;
            let mut profile = config.profiles.entry(args.profile.id).or_default();
            profile.name = args.user.name;
            config.save(path.as_ref())?;
        }
        Applet::DCA(DCA::Client(Client::Key(Key::Generate(args)))) | Applet::GenerateKey(args) => {
            let path = args.config.path();
            let mut config = Config::load(path.as_ref())?;

            let password = secrets::get_password(
                &mut config,
                #[cfg(feature = "secret-service")]
                args.skip_secret_service,
            )
            .into_diagnostic()?;

            let mut profile = config.profiles.entry(args.profile.id).or_default();

            // TODO: Move key generation into secrets module
            log::info!("Generating RSA key");
            let mut rng = rand::rngs::OsRng;
            let private_key = rsa::RsaPrivateKey::new(&mut rng, 2048).into_diagnostic()?;
            let public_key = rsa::RsaPublicKey::from(&private_key);

            // TODO: We need RustCrypto/format prelease for pkcs7
            // but RustCrypto/RSA hasn't been updated to prerelease,
            // so we're format shifting and hoping it gets optimised away.
            let private_der = pkcs8::PrivateKeyDocument::from_pkcs8_der(
                private_key.to_pkcs8_der().into_diagnostic()?.as_ref(),
            )
            .map_err(|e| miette!("Decode key from der failed: {}", e))?;

            log::info!("Encrypting RSA key");
            let private_encrypted_der = private_der
                .encrypt(&mut rng, &password)
                .map_err(|e| miette!("Encrypt private key failed: {}", e))?;
            let public_der = pkcs8::PublicKeyDocument::from_public_key_der(
                public_key.to_public_key_der().into_diagnostic()?.as_ref(),
            )
            .into_diagnostic()?;
            profile.key = Some(config::KeyInfo {
                private_key: private_encrypted_der,
                public_key: public_der,
            });

            config.save(path.as_ref())?;
        }
        Applet::DCA(DCA::Client(Client::Key(Key::Get(args)))) | Applet::GetKey(args) => {
            let path = args.config.path();
            let config = Config::load(path.as_ref())?;

            let profile = config
                .profiles
                .get(&args.profile.id)
                .ok_or_else(|| miette!("Profile {} not found", &args.profile.id))?;
            let public_key = &profile
                .key
                .as_ref()
                .ok_or_else(|| miette!("Key not found"))?
                .public_key;
            // TODO: Flag issue upstream that MapTool serialised public key is not PEM
            print!("{}", maptool::format_pubkey(&public_key));
        }
        Applet::DCA(DCA::Client(Client::Key(Key::Delete(args)))) | Applet::DeleteKey(args) => {
            let path = args.config.path();
            let mut config = Config::load(path.as_ref())?;

            let mut profile = config.profiles.entry(args.profile.id);
            match profile {
                Entry::Vacant(v) => {
                    if !args.force {
                        bail!("Profile {} not previously defined", v.key());
                    }
                }
                Entry::Occupied(ref mut o) => {
                    let profile = o.get_mut();
                    match profile.key {
                        Some(_) => {
                            profile.key = None;
                            config.save(path.as_ref())?;
                        }
                        None => {
                            if !args.force {
                                bail!("Profile {} has no key to delete", o.key());
                            }
                        }
                    }
                }
            }
        }
        Applet::DCA(DCA::Client(Client::Key(Key::Test(KeyTest::Encrypt(args))))) => {
            let path = args.config.path();
            let mut config = Config::load(path.as_ref())?;

            let profile = config.profiles.entry(args.profile.id).or_default();
            let public_key = &profile
                .key
                .as_ref()
                .ok_or_else(|| miette!("Key not found"))?
                .public_key;
            // TODO: Unify types when ecosystem settles
            let public_key = RsaPublicKey::from_public_key_der(public_key.as_ref())
                .expect("RsaPublicKey should be interchangeable with PublicKeyDocument");
            let input = read_input(args.input)?;
            let mut rng = rand::rngs::OsRng;
            let encrypted = public_key
                .encrypt(
                    &mut rng,
                    rsa::padding::PaddingScheme::PKCS1v15Encrypt,
                    &input,
                )
                .into_diagnostic()?;
            write_output(args.output, &encrypted)?;
        }
        Applet::DCA(DCA::Client(Client::Key(Key::Test(KeyTest::Decrypt(args))))) => {
            let path = args.config.path();
            let mut config = Config::load(path.as_ref())?;

            let app_password = secrets::get_password(
                &mut config,
                #[cfg(feature = "secret-service")]
                args.skip_secret_service,
            )
            .into_diagnostic()?;

            let profile = config.profiles.entry(args.profile.id).or_default();
            let encrypted_private_key = &profile
                .key
                .as_ref()
                .ok_or_else(|| miette!("Key not found"))?
                .private_key;
            log::info!("Decrypting private key");
            let private_key = encrypted_private_key
                .decrypt(&app_password)
                .into_diagnostic()?;
            // TODO: Unify types when ecosystem settles
            let private_key = RsaPrivateKey::from_pkcs8_der(private_key.as_ref())
                .expect("RsaPublicKey should be interchangeable with PublicKeyDocument");
            let input = read_input(args.input)?;
            log::info!("Decrypting input");
            let decrypted = private_key
                .decrypt(rsa::padding::PaddingScheme::PKCS1v15Encrypt, &input)
                .into_diagnostic()?;
            write_output(args.output, &decrypted)?;
        }
        Applet::DCA(DCA::Client(Client::Key(Key::Test(KeyTest::Sign(args))))) => {
            let path = args.config.path();
            let mut config = Config::load(path.as_ref())?;

            let app_password = secrets::get_password(
                &mut config,
                #[cfg(feature = "secret-service")]
                args.skip_secret_service,
            )
            .into_diagnostic()?;

            let profile = config.profiles.entry(args.profile.id).or_default();
            let encrypted_private_key = &profile
                .key
                .as_ref()
                .ok_or_else(|| miette!("Key not found"))?
                .private_key;
            log::info!("Decrypting private key");
            let private_key = encrypted_private_key
                .decrypt(&app_password)
                .into_diagnostic()?;
            // TODO: Unify types when ecosystem settles
            let private_key = RsaPrivateKey::from_pkcs8_der(private_key.as_ref())
                .expect("RsaPublicKey should be interchangeable with PublicKeyDocument");
            log::info!("Hashing input");
            let input = read_input(args.input)?;
            use sha1::Digest;
            let hash = sha1::Sha1::digest(&input);
            log::info!("Signing hash");
            let signature = private_key
                .sign(
                    rsa::padding::PaddingScheme::PKCS1v15Sign {
                        hash: Some(rsa::Hash::SHA1),
                    },
                    &hash,
                )
                .into_diagnostic()?;
            // TODO: This allocates when it ought not to need to
            print!(
                "hash: {}\nsignature: {}\n",
                hex::encode(&hash),
                hex::encode(signature)
            );
        }
        Applet::DCA(DCA::Client(Client::Key(Key::Test(KeyTest::Verify(args))))) => {
            let path = args.config.path();
            let config = Config::load(path.as_ref())?;

            let profile = config
                .profiles
                .get(&args.profile.id)
                .ok_or_else(|| miette!("Profile {} not found", &args.profile.id))?;
            let public_key = &profile
                .key
                .as_ref()
                .ok_or_else(|| miette!("Key not found"))?
                .public_key;
            // TODO: Unify types when ecosystem settles
            let public_key = RsaPublicKey::from_public_key_der(public_key.as_ref())
                .expect("RsaPublicKey should be interchangeable with PublicKeyDocument");
            public_key
                .verify(
                    rsa::padding::PaddingScheme::PKCS1v15Sign {
                        hash: Some(rsa::Hash::SHA1),
                    },
                    &args.hash,
                    &args.signature,
                )
                .into_diagnostic()?;
        }
        Applet::DCA(DCA::Server(Server::Kind(ServerKind::Set(args)))) => {
            let path = args.config.path();
            let mut config = Config::load(path.as_ref())?;

            let profile = config.profiles.entry(args.profile.id).or_default();
            match profile.servers.entry(args.server) {
                Entry::Vacant(v) => {
                    v.insert(args.kind.into());
                }
                Entry::Occupied(o) => {
                    let (k, v) = o.remove_entry();
                    profile.servers.insert(k, v.into_kind(args.kind));
                }
            };
            config.save(path.as_ref())?;
        }
        Applet::DCA(DCA::Server(Server::Password(Password::Set(args))))
        | Applet::SetPassword(args) => {
            let path = args.config.path();
            let mut config = Config::load(path.as_ref())?;

            let app_password = secrets::get_password(
                &mut config,
                #[cfg(feature = "secret-service")]
                args.skip_secret_service,
            )
            .into_diagnostic()?;

            let profile_id = args.profile.id;
            let server_name = args.server;
            let profile = config.profiles.entry(profile_id).or_default();
            let server = profile
                .servers
                .get_mut(&server_name)
                .ok_or_else(|| miette!("Server {} not found", &server_name))?;
            if args.no_clobber && server.get_password_mut().is_some() {
                bail!("Server {} already has a password", &server_name);
            }

            log::info!("Decrypting server password");
            let server_password = secrets::get_server_password(&server_name).into_diagnostic()?;

            log::info!("Encrypting password");
            let pem = secrets::encrypt_server_password(&server_password, &app_password)?;

            *server.get_password_mut() = Some(pem);

            config.save(path.as_ref())?;
        }
        Applet::DCA(DCA::Server(Server::Password(Password::Delete(args))))
        | Applet::DeletePassword(args) => {
            let path = args.config.path();
            let mut config = Config::load(path.as_ref())?;

            let profile = config.profiles.entry(args.profile.id).or_default();
            let server = match profile.servers.get_mut(&args.server) {
                None => {
                    if args.force {
                        return Ok(());
                    }
                    bail!("Server {} not found", &args.server);
                }
                Some(server) => server,
            };

            let password = server.get_password_mut();
            if password.is_none() {
                if args.force {
                    return Ok(());
                }
                bail!("No password found for {}", &args.server);
            }

            *password = None;
            config.save(path.as_ref())?;
        }
        Applet::DCA(DCA::Server(Server::Password(Password::Test(args))))
        | Applet::TestPassword(args) => {
            let path = args.config.path();
            let mut config = Config::load(path.as_ref())?;

            let app_password = secrets::get_password(
                &mut config,
                #[cfg(feature = "secret-service")]
                args.skip_secret_service,
            )
            .into_diagnostic()?;

            let profile_id = args.profile.id;
            let server_name = args.server;
            let profile = config.profiles.entry(profile_id).or_default();
            let server = profile
                .servers
                .get(&server_name)
                .ok_or_else(|| miette!("Server {} not found", &server_name))?;
            let pem = server
                .get_password()
                .as_ref()
                .ok_or_else(|| miette!("No password found for {}", &server_name))?;
            log::info!("Decrypting server password");
            let password = secrets::decrypt_server_password(&pem, &app_password)?;

            // TODO: The pkcs5 crate doesn't expose an API for deriving the encryption key
            // or a constructed cipher, just *crypt this buffer.
            // This is what we want for MapTool but testing requires reading the input into memory
            // rather than streaming input.
            let parameters = secrets::pbkdf2_with_hmac_sha1_aes128cbc(&args.salt, &args.iv);

            let mut buf = read_input(args.input)?;

            // TODO: Nicer reporting
            let output_len = if !args.decrypt {
                log::info!("encrypting input");
                const AES_BLOCK_SIZE: usize = 16;
                let input_len = buf.len();
                buf.extend_from_slice(&[0; AES_BLOCK_SIZE]);
                parameters
                    .encrypt_in_place(password, &mut buf, input_len)
                    .map_err(|e| miette!("Unable to encrypt data: {}", e))?
                    .len()
            } else {
                log::info!("decrypting input");
                parameters
                    .decrypt_in_place(password, &mut buf)
                    .map_err(|e| miette!("Unable to decrypt data: {}", e))?
                    .len()
            };
            buf.truncate(output_len);

            write_output(args.output, &buf)?;
        }
        Applet::DCA(DCA::Connection) => unimplemented!(),
        Applet::DCA(DCA::State) => unimplemented!(),
        Applet::DCA(DCA::Sheet) => unimplemented!(),
        Applet::DCA(DCA::HelloWorld(args)) => {
            let path = args.config.path();
            let mut config = Config::load(path.as_ref())?;

            let app_password = secrets::get_password(
                &mut config,
                #[cfg(feature = "secret-service")]
                args.skip_secret_service,
            )
            .into_diagnostic()?;

            let profile_id = args.profile.id;
            let server_name = args.server;
            let profile = match config.profiles.entry(profile_id) {
                Entry::Vacant(_) => Default::default(),
                Entry::Occupied(o) => o.remove(),
            };

            let rt = tokio::runtime::Builder::new_current_thread()
                .enable_io()
                .enable_time()
                .build()
                .into_diagnostic()?;
            rt.block_on(async {
                let server = profile
                    .servers
                    .get(&server_name)
                    .ok_or_else(|| miette!("Server {} not found", &server_name))?;
                log::info!("Connecting to server");

                let server_address = match server {
                    config::ServerInfo::Direct(server) => {
                        ServerAddress::Direct(DirectServerAddress {
                            address: server.address.as_ref().unwrap_or(&server_name),
                        })
                    }
                    config::ServerInfo::RegistryWebSocket(server) => {
                        ServerAddress::WebRtc(WebRtcServerAddress {
                            name: profile.name.as_str(),
                            server_name: server
                                .server_name
                                .as_ref()
                                .unwrap_or(&server_name)
                                .as_str(),
                        })
                    }
                    _ => unimplemented!(),
                };
                let conn = maptool::ServerConnection::connect(server_address).await?;

                log::info!("Initiating handshake");

                fn report_pin_fn(
                    pin: &str,
                ) -> impl Future<Output = Result<(), core::convert::Infallible>> {
                    println!(
                        "Server has not previously registered public key. \
                                 Sending public key to server. \
                                 Let the GM know your PIN is {}",
                        pin
                    );
                    async { Ok(()) }
                }
                let app_password = &app_password;
                let (connection_successful_msg, mut session) = maptool::handshake::handshake(
                    conn.compat(),
                    profile.name.into(),
                    profile.key.map(|key_info| {
                        let public_key = key_info.public_key;
                        let private_key = key_info.private_key;
                        let private_key_fn = move || async move {
                            log::info!("Decrypting private key");
                            let private_key_document = private_key.decrypt(app_password)?;
                            Ok::<_, pkcs8::Error>(private_key_document)
                        };
                        maptool::handshake::KeyOptions {
                            public_key,
                            report_pin_fn,
                            private_key_fn,
                        }
                    }),
                    server.get_password().as_ref().map(|pem| {
                        move || async move {
                            log::info!("Decrypting server password");
                            secrets::decrypt_server_password(&pem, app_password)
                        }
                    }),
                )
                .await?;
                log::debug!("{:?}", connection_successful_msg);

                let mut buf = Vec::new();
                loop {
                    buf = session.read_msg(buf).await?;
                    println!("{:?}", &buf);
                }
                Ok::<_, miette::Error>(())
            })?
        }
        Applet::Decay => println!("decay"),
        Applet::UnrecognisedApplet(args) => {
            log::debug!("{:?}", args);
        }
    }
    Ok(())
}
