use std::{collections::btree_map::BTreeMap, fs, io, path::Path};

use miette::Diagnostic;
use serde::{Deserialize, Serialize};
use thiserror::Error;
use uuid::Uuid;

use crate::pkcs7_content_info::ContentInfoDocument;
use pkcs8::{EncryptedPrivateKeyDocument, PublicKeyDocument};

pub(crate) type Salt = u32;
pub(crate) type Hash = sha2::Sha256;
pub(crate) type HashDigest = sha2::digest::Output<Hash>;

mod hash_base64 {
    use super::HashDigest;
    use serde::{Deserialize, Deserializer, Serializer};

    pub(crate) fn serialize<T, S>(key: &T, serializer: S) -> Result<S::Ok, S::Error>
    where
        T: AsRef<[u8]>,
        S: Serializer,
    {
        serializer.serialize_str(&base64::encode(key.as_ref()))
    }

    pub(crate) fn deserialize<'de, D: Deserializer<'de>>(d: D) -> Result<HashDigest, D::Error> {
        use std::convert::TryInto;
        let s = String::deserialize(d)?;
        let v = base64::decode(s.as_bytes()).map_err(|e| serde::de::Error::custom(e))?;
        let a: [_; 32] = v
            .try_into()
            .map_err(|_| serde::de::Error::custom("Array incorrect length"))?;
        let d = a.into();
        Ok(d)
    }
}

#[derive(Debug, Deserialize, Serialize)]
pub(crate) struct AppPasswordInfo {
    #[serde(with = "hash_base64")]
    pub(crate) hash: sha2::digest::Output<sha2::Sha256>,
    pub(crate) salt: Salt,
}

mod as_pem {
    use serde::{Deserialize, Deserializer, Serializer};
    pub(crate) fn deserialize<'a, 'de, D, T>(deserializer: D) -> Result<T, D::Error>
    where
        D: Deserializer<'de>,
        T: der::Document<'a> + pem_rfc7468::PemLabel,
    {
        let pem = String::deserialize(deserializer)?;
        T::from_pem(&pem).map_err(serde::de::Error::custom)
    }

    pub(crate) fn serialize<'a, S, T>(value: &T, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
        T: der::Document<'a> + pem_rfc7468::PemLabel,
    {
        let pem = value
            .to_pem(Default::default())
            .map_err(serde::ser::Error::custom)?;
        serializer.serialize_str(&pem)
    }
}

#[derive(Debug, Deserialize, Serialize)]
pub(crate) struct KeyInfo {
    #[serde(with = "as_pem")]
    pub(crate) private_key: EncryptedPrivateKeyDocument,
    #[serde(with = "as_pem")]
    pub(crate) public_key: PublicKeyDocument,
}

#[derive(Debug, Default, Deserialize, Serialize)]
pub(crate) struct DirectServer {
    // from alias
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) address: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) password: Option<ContentInfoDocument>,
}

#[derive(Debug, Default, Deserialize, Serialize)]
pub(crate) struct LocalServer {
    // from alias
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) server_name: Option<String>,
    #[serde(skip_serializing_if = "Vec::is_empty")]
    pub(crate) interfaces: Vec<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) password: Option<ContentInfoDocument>,
}

#[derive(Debug, Default, Deserialize, Serialize)]
pub(crate) struct RegistryServer {
    // from alias
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) server_name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) password: Option<ContentInfoDocument>,
}

impl From<DirectServer> for LocalServer {
    fn from(server: DirectServer) -> Self {
        Self {
            password: server.password,
            ..Default::default()
        }
    }
}
impl From<DirectServer> for RegistryServer {
    fn from(server: DirectServer) -> Self {
        Self {
            password: server.password,
            ..Default::default()
        }
    }
}

impl From<LocalServer> for DirectServer {
    fn from(server: LocalServer) -> Self {
        Self {
            password: server.password,
            ..Default::default()
        }
    }
}
impl From<LocalServer> for RegistryServer {
    fn from(server: LocalServer) -> Self {
        Self {
            password: server.password,
            ..Default::default()
        }
    }
}

impl From<RegistryServer> for DirectServer {
    fn from(server: RegistryServer) -> Self {
        Self {
            password: server.password,
            ..Default::default()
        }
    }
}
impl From<RegistryServer> for LocalServer {
    fn from(server: RegistryServer) -> Self {
        Self {
            password: server.password,
            ..Default::default()
        }
    }
}

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "kebab-case", tag = "kind")]
pub(crate) enum ServerInfo {
    Direct(DirectServer),
    Local(LocalServer),
    RegistryAuto(RegistryServer),
    RegistryHTTP(RegistryServer),
    RegistryWebSocket(RegistryServer),
}

#[derive(clap::ArgEnum, Clone, Debug, Deserialize, Serialize)]
pub(crate) enum ServerKind {
    Direct,
    Local,
    RegistryAuto,
    RegistryHTTP,
    RegistryWebSocket,
}

impl From<DirectServer> for ServerInfo {
    fn from(server: DirectServer) -> Self {
        Self::Direct(server)
    }
}

impl From<LocalServer> for ServerInfo {
    fn from(server: LocalServer) -> Self {
        Self::Local(server)
    }
}

impl ServerInfo {
    /// Lossily converts a Server to a different kind
    pub(crate) fn into_kind(self, kind: ServerKind) -> Self {
        match (self, kind) {
            (ServerInfo::Direct(server), ServerKind::Direct) => server.into(),
            (ServerInfo::Direct(server), ServerKind::Local) => Self::Local(server.into()),
            (ServerInfo::Direct(server), ServerKind::RegistryAuto) => Self::RegistryAuto(server.into()),
            (ServerInfo::Direct(server), ServerKind::RegistryHTTP) => Self::RegistryHTTP(server.into()),
            (ServerInfo::Direct(server), ServerKind::RegistryWebSocket) => Self::RegistryWebSocket(server.into()),
            (ServerInfo::Local(server), ServerKind::Direct) => Self::Direct(server.into()),
            (ServerInfo::Local(server), ServerKind::Local) => server.into(),
            (ServerInfo::Local(server), ServerKind::RegistryAuto) => Self::RegistryAuto(server.into()),
            (ServerInfo::Local(server), ServerKind::RegistryHTTP) => Self::RegistryHTTP(server.into()),
            (ServerInfo::Local(server), ServerKind::RegistryWebSocket) => Self::RegistryWebSocket(server.into()),
            (ServerInfo::RegistryAuto(server), ServerKind::Direct) => Self::Direct(server.into()),
            (ServerInfo::RegistryAuto(server), ServerKind::Local) => Self::Local(server.into()),
            (ServerInfo::RegistryAuto(server), ServerKind::RegistryAuto) => Self::RegistryAuto(server),
            (ServerInfo::RegistryAuto(server), ServerKind::RegistryHTTP) => Self::RegistryHTTP(server),
            (ServerInfo::RegistryAuto(server), ServerKind::RegistryWebSocket) => Self::RegistryWebSocket(server),
            (ServerInfo::RegistryHTTP(server), ServerKind::Direct) => Self::Direct(server.into()),
            (ServerInfo::RegistryHTTP(server), ServerKind::Local) => Self::Local(server.into()),
            (ServerInfo::RegistryHTTP(server), ServerKind::RegistryAuto) => Self::RegistryAuto(server),
            (ServerInfo::RegistryHTTP(server), ServerKind::RegistryHTTP) => Self::RegistryHTTP(server),
            (ServerInfo::RegistryHTTP(server), ServerKind::RegistryWebSocket) => Self::RegistryWebSocket(server),
            (ServerInfo::RegistryWebSocket(server), ServerKind::Direct) => Self::Direct(server.into()),
            (ServerInfo::RegistryWebSocket(server), ServerKind::Local) => Self::Local(server.into()),
            (ServerInfo::RegistryWebSocket(server), ServerKind::RegistryAuto) => Self::RegistryAuto(server),
            (ServerInfo::RegistryWebSocket(server), ServerKind::RegistryHTTP) => Self::RegistryHTTP(server),
            (ServerInfo::RegistryWebSocket(server), ServerKind::RegistryWebSocket) => Self::RegistryWebSocket(server),
        }
    }
    pub(crate) fn get_password_mut(&mut self) -> &mut Option<ContentInfoDocument> {
        match self {
            ServerInfo::Direct(server) => &mut server.password,
            ServerInfo::Local(server) => &mut server.password,
            ServerInfo::RegistryAuto(server) => &mut server.password,
            ServerInfo::RegistryHTTP(server) => &mut server.password,
            ServerInfo::RegistryWebSocket(server) => &mut server.password,
        }
    }
    pub(crate) fn get_password(&self) -> & Option<ContentInfoDocument> {
        match self {
            ServerInfo::Direct(server) => &server.password,
            ServerInfo::Local(server) => &server.password,
            ServerInfo::RegistryAuto(server) => &server.password,
            ServerInfo::RegistryHTTP(server) => &server.password,
            ServerInfo::RegistryWebSocket(server) => &server.password,
        }
    }
}

impl From<ServerKind> for ServerInfo {
    fn from(kind: ServerKind) -> Self {
        match kind {
            ServerKind::Direct => ServerInfo::Direct(Default::default()),
            ServerKind::Local => ServerInfo::Local(Default::default()),
            ServerKind::RegistryAuto => ServerInfo::RegistryAuto(Default::default()),
            ServerKind::RegistryHTTP => ServerInfo::RegistryHTTP(Default::default()),
            ServerKind::RegistryWebSocket => ServerInfo::RegistryWebSocket(Default::default()),
        }
    }
}

#[derive(Debug, Deserialize, Serialize)]
pub(crate) struct ProfileInfo {
    pub(crate) client_id: String,
    pub(crate) name: String,
    #[serde(flatten, skip_serializing_if = "Option::is_none")]
    pub(crate) key: Option<KeyInfo>,
    #[serde(default, rename = "server", skip_serializing_if = "BTreeMap::is_empty")]
    pub(crate) servers: BTreeMap<String, ServerInfo>,
}

impl Default for ProfileInfo {
    fn default() -> Self {
        Self {
            client_id: Uuid::new_v4().to_hyphenated().to_string(),
            name: Default::default(),
            key: Default::default(),
            servers: Default::default(),
        }
    }
}

#[derive(Debug, Default, Deserialize, Serialize)]
pub(crate) struct Config {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) password: Option<AppPasswordInfo>,
    #[serde(rename = "profile")]
    pub(crate) profiles: BTreeMap<String, ProfileInfo>,
}

#[derive(Debug, Diagnostic, Error)]
pub(crate) enum ConfigLoadError {
    #[error("Config unreadable")]
    ReadError(#[from] io::Error),
    #[error("Config undeserializable")]
    DeserializeError(#[from] toml::de::Error),
}

#[derive(Debug, Diagnostic, Error)]
pub(crate) enum ConfigSaveError {
    #[error("Config unserializable")]
    SerializeError(#[from] toml::ser::Error),
    #[error("Config unwritable")]
    WriteError(#[from] io::Error),
}

impl Config {
    pub(crate) fn load(path: &Path) -> Result<Self, ConfigLoadError> {
        let config: Config = match fs::read(path) {
            Ok(contents) => toml::from_slice(&contents)?,
            Err(e) => match e.kind() {
                io::ErrorKind::NotFound => Default::default(),
                _ => return Err(e.into()),
            },
        };
        Ok(config)
    }

    pub(crate) fn save(&self, path: &Path) -> Result<(), ConfigSaveError> {
        Ok(fs::write(path, toml::ser::to_string_pretty(self)?)?)
    }
}
