use core::{fmt::Display, str::FromStr};
use std::{error::Error as StdError, net::ToSocketAddrs};

use thiserror::Error;
pub use zbus;
use zbus::{dbus_proxy, fdo, zvariant::{Optional, Type}};

// TODO: Can't do this, can go the other way around but that needs another crate
use maptool_types::{DirectServerAddress, ServerAddress, WebRtcServerAddress};

#[derive(Clone, Debug, serde::Deserialize, serde::Serialize, Type)]
#[cfg_attr(feature = "clap", derive(clap::Args))]
pub struct OwnedDirectServerAddress<DirectAddress>
where
    DirectAddress: ToSocketAddrs + FromStr + Type,
    <DirectAddress as FromStr>::Err: Display + Send + Sync + 'static + StdError,
{
    pub address: DirectAddress,
}

impl<'a, DirectAddress> Into<DirectServerAddress<&'a DirectAddress>>
    for &'a OwnedDirectServerAddress<DirectAddress>
where
    DirectAddress: ToSocketAddrs + FromStr + Type,
    <DirectAddress as FromStr>::Err: Display + Send + Sync + 'static + StdError,
{
    fn into(self) -> DirectServerAddress<&'a DirectAddress> {
        DirectServerAddress {
            address: &self.address,
        }
    }
}

#[derive(Clone, Debug, serde::Deserialize, serde::Serialize, Type)]
#[cfg_attr(feature = "clap", derive(clap::Args))]
pub struct OwnedWebRtcServerAddress<Name, ServerName>
where
    Name: AsRef<str> + FromStr + Type,
    <Name as FromStr>::Err: Display + Send + Sync + 'static + StdError,
    ServerName: AsRef<str> + FromStr + Type,
    <ServerName as FromStr>::Err: Display + Send + Sync + 'static + StdError,
{
    name: Name,
    server_name: ServerName,
}

impl<'a, Name, ServerName> Into<WebRtcServerAddress<&'a Name, &'a ServerName>>
    for &'a OwnedWebRtcServerAddress<Name, ServerName>
where
    Name: AsRef<str> + FromStr + Type,
    <Name as FromStr>::Err: Display + Send + Sync + 'static + StdError,
    ServerName: AsRef<str> + FromStr + Type,
    <ServerName as FromStr>::Err: Display + Send + Sync + 'static + StdError,
{
    fn into(self) -> WebRtcServerAddress<&'a Name, &'a ServerName> {
        WebRtcServerAddress {
            name: &self.name,
            server_name: &self.server_name,
        }
    }
}

#[derive(Clone, Debug)]
#[cfg_attr(feature = "clap", derive(clap::Parser))]
#[cfg_attr(feature = "serde", derive(serde::Deserialize, serde::Serialize))]
pub enum OwnedServerAddress<DirectAddress, Name, ServerName>
where
    DirectAddress: ToSocketAddrs + FromStr + Type,
    <DirectAddress as FromStr>::Err: Display + Send + Sync + 'static + StdError,
    Name: AsRef<str> + FromStr + Type,
    <Name as FromStr>::Err: Display + Send + Sync + 'static + StdError,
    ServerName: AsRef<str> + FromStr + Type,
    <ServerName as FromStr>::Err: Display + Send + Sync + 'static + StdError,
{
    Direct(OwnedDirectServerAddress<DirectAddress>),
    WebRtc(OwnedWebRtcServerAddress<Name, ServerName>),
}

impl<'a, DirectAddress, Name, ServerName>
    Into<ServerAddress<&'a DirectAddress, &'a Name, &'a ServerName>>
    for &'a OwnedServerAddress<DirectAddress, Name, ServerName>
where
    DirectAddress: ToSocketAddrs + FromStr + Type,
    <DirectAddress as FromStr>::Err: Display + Send + Sync + 'static + StdError,
    Name: AsRef<str> + FromStr + Type,
    <Name as FromStr>::Err: Display + Send + Sync + 'static + StdError,
    ServerName: AsRef<str> + FromStr + Type,
    <ServerName as FromStr>::Err: Display + Send + Sync + 'static + StdError,
{
    fn into(self) -> ServerAddress<&'a DirectAddress, &'a Name, &'a ServerName> {
        match self {
            OwnedServerAddress::Direct(addr) => ServerAddress::Direct(addr.into()),
            OwnedServerAddress::WebRtc(addr) => ServerAddress::WebRtc(addr.into()),
        }
    }
}

#[derive(Debug, zbus::DBusError)]
#[cfg_attr(feature = "miette", derive(miette::Diagnostic))]
#[dbus_error(prefix = "io.gitlab.richardmaw.MapToolAgent")]
pub enum SubscribeError {
    #[dbus_error(zbus_error)]
    ZBus(zbus::Error),
    NoPeerUniqueName,
}

#[derive(Debug, zbus::DBusError)]
#[cfg_attr(feature = "miette", derive(miette::Diagnostic))]
#[dbus_error(prefix = "io.gitlab.richardmaw.MapToolAgent")]
pub enum SendRawMessageError {
    #[dbus_error(zbus_error)]
    ZBus(zbus::Error),
    WriteMsgError(String),
}

#[derive(Clone, Debug, Error, serde::Deserialize, serde::Serialize, Type)]
pub enum MessageTaskReadError {
    #[error("Failed to read disconnection")]
    NameLostSignalReadError,
    #[error("Failed to read session message")]
    ReadError,
    #[error("Failed to write session message")]
    WriteError,
}

#[dbus_proxy(
    interface = "io.gitlab.richardmaw.MapToolAgent.Session1",
    default_service = "io.gitlab.richardmaw.MapToolAgent"
)]
pub trait MapToolAgentSession1 {
    fn subscribe(&self) -> Result<(), SubscribeError>;

    #[dbus_proxy(signal)]
    fn raw_message(&self, message: Vec<u8>) -> fdo::Result<()>;

    #[dbus_proxy(signal)]
    fn read_error(&self, error: MessageTaskReadError) -> fdo::Result<()>;

    fn send_raw_message(&self, message: Vec<u8>) -> Result<(), SendRawMessageError>;

    fn disconnect(&self) -> fdo::Result<()>;
}

#[derive(Debug, zbus::DBusError)]
#[cfg_attr(feature = "miette", derive(miette::Diagnostic))]
#[dbus_error(prefix = "io.gitlab.richardmaw.MapToolAgent")]
pub enum ConnectionHandshakeError {
    #[dbus_error(zbus_error)]
    ZBus(zbus::Error),
    AlreadyConnected,
    HandshakeError(String),
}

#[derive(Debug, zbus::DBusError)]
#[cfg_attr(feature = "miette", derive(miette::Diagnostic))]
#[dbus_error(prefix = "io.gitlab.richardmaw.MapToolAgent")]
pub enum ConnectionDisconnectError {
    #[dbus_error(zbus_error)]
    ZBus(zbus::Error),
    AlreadyConnected,
}

#[dbus_proxy(
    interface = "io.gitlab.richardmaw.MapToolAgent.Connection1",
    default_service = "io.gitlab.richardmaw.MapToolAgent"
)]
pub trait MapToolAgentConnection1 {
    /// Sent during handshake if the server is using EasyConnect and the user hasn't been
    /// previously registered.
    #[dbus_proxy(signal)]
    fn pin(&self, pin: &str) -> fdo::Result<()>;

    /// Perform the handshake with provided password or keys
    /// depending on server's config.
    /// If the server requires credentials that weren't provided
    /// then an error is returned.
    ///
    /// Secret credentials are passed unencrypted,
    /// so are vulnerable to credential snooping
    /// and MITM attacks.
    /// However until MapTool switches over entirely to WebRTC
    /// with a wss:// signalling server and the server authenticates itself
    /// a MITM attack there is more valuable.
    #[dbus_proxy(object = "MapToolAgentSession1")]
    fn handshake(
        &self,
        name: &str,
        password: &Optional<Vec<u8>>,
        public_key: &Optional<Vec<u8>>,
        private_key: &Optional<Vec<u8>>,
    ) -> Result<zbus::OwnedObjectPath, ConnectionHandshakeError>;

    fn disconnect(&self) -> Result<(), ConnectionDisconnectError>;
}

#[derive(Debug, zbus::DBusError)]
#[cfg_attr(feature = "miette", derive(miette::Diagnostic))]
#[dbus_error(prefix = "io.gitlab.richardmaw.MapToolAgent")]
pub enum ManagerConnectError {
    #[dbus_error(zbus_error)]
    ZBus(zbus::Error),
    ConnectError(String),
}

#[dbus_proxy(
    interface = "io.gitlab.richardmaw.MapToolAgent.Manager1",
    default_service = "io.gitlab.richardmaw.MapToolAgent",
    default_path = "/io/gitlab/richardmaw/MapToolAgent"
)]
pub trait MapToolAgentManager1 {
    #[dbus_proxy(object = "MapToolAgentConnection1")]
    fn connect_direct(
        &self,
        address: OwnedDirectServerAddress<String>,
    ) -> Result<zbus::OwnedObjectPath, ManagerConnectError>;
    #[dbus_proxy(object = "MapToolAgentConnection1")]
    fn connect_webrtc(
        &self,
        address: OwnedWebRtcServerAddress<String, String>,
    ) -> Result<zbus::OwnedObjectPath, ManagerConnectError>;
    fn shutdown(&self) -> fdo::Result<()>;
    fn source_offer(&self) -> fdo::Result<Vec<u8>>;
}
