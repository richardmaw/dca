use std::net::ToSocketAddrs;

pub struct DirectServerAddress<DirectAddress>
where
    DirectAddress: ToSocketAddrs,
{
    pub address: DirectAddress,
}

pub struct WebRtcServerAddress<Name, ServerName>
where
    Name: AsRef<str>,
    ServerName: AsRef<str>,
{
    pub name: Name,
    pub server_name: ServerName,
}

pub enum ServerAddress<DirectAddress, Name, ServerName>
where
    DirectAddress: ToSocketAddrs,
    Name: AsRef<str>,
    ServerName: AsRef<str>,
{
    Direct(DirectServerAddress<DirectAddress>),
    WebRtc(WebRtcServerAddress<Name, ServerName>),
}

impl<'a, DirectAddress, Name, ServerName> From<&'a DirectServerAddress<DirectAddress>>
    for ServerAddress<&'a DirectAddress, Name, ServerName>
where
    DirectAddress: ToSocketAddrs,
    Name: AsRef<str>,
    ServerName: AsRef<str>,
{
    fn from(server_address: &'a DirectServerAddress<DirectAddress>) -> Self {
        Self::Direct(DirectServerAddress {
            address: &server_address.address,
        })
    }
}

impl<'a, DirectAddress, Name, ServerName> From<&'a WebRtcServerAddress<Name, ServerName>>
    for ServerAddress<DirectAddress, &'a Name, &'a ServerName>
where
    DirectAddress: ToSocketAddrs,
    Name: AsRef<str>,
    ServerName: AsRef<str>,
{
    fn from(server_address: &'a WebRtcServerAddress<Name, ServerName>) -> Self {
        Self::WebRtc(WebRtcServerAddress {
            name: &server_address.name,
            server_name: &server_address.server_name,
        })
    }
}
