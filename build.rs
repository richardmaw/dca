use std::env;

fn main() {
    println!("cargo:rerun-if-env-changed=ENV_PREFIX");
    println!(
        "cargo:rustc-env=ENV_PREFIX={}",
        env::var("ENV_PREFIX").unwrap_or("".to_owned())
    );
    println!("cargo:rerun-if-env-changed=PROGRAM_PREFIX");
    println!(
        "cargo:rustc-env=PROGRAM_PREFIX={}",
        env::var("PROGRAM_PREFIX").unwrap_or("".to_owned())
    );
    println!("cargo:rerun-if-env-changed=PREFIX");
    let prefix = env::var("PREFIX").unwrap_or("/usr/local".to_owned());
    println!("cargo:rustc-env=PREFIX={}", prefix);
    println!("cargo:rerun-if-env-changed=BINDIR");
    println!(
        "cargo:rustc-env=BINDIR={}",
        env::var("BINDIR").unwrap_or(format!("{}/bin", prefix))
    );
}
