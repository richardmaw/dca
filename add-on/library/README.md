# dca-add-on-lib

## License

Decay is not provided under an Open Source License.
It is instead provided with a non-commercial license,
the [AFPL][], and available to all under personal use.

Unlike its creators,
whose intent was to force commercial users to pay for development,
no dual-licensing will be provided.

This is because the project is developed as a hobby
and a preference that work done in my own time remains forever non-commercial,
but also it was developed with intent to use with GURPS,
and if any GURPS specific behaviour is included the tool MUST be non-commercial.

[AFPL]: https://tldrlegal.com/license/aladdin-free-public-license#summary
